package simulator

import (
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestMain(m *testing.M) {
	
	os.Exit(m.Run())
}

func TestSimulator(t *testing.T) {
	simulator := NewSimulator()
	simulator.Up()
	outputDevice := simulator.GetOutputDevice()
	output := outputDevice.GetOutput()
	assert.Equal(t, "Welcome to Computer Simulator!", output)
}
