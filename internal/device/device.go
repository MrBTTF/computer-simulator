package device

import "component"

type CPU struct {
	registers            [4]*component.Register
	ram                  *component.RAM
	alu                  *component.ALU
	tmp                  *component.Register
	acc                  *component.Register
	instrRegister        *component.Register
	instrAddressRegister *component.Register
	controlSection       *ControlSection
	bus                  *Bus
	clock                chan bool
	run                  bool
}

func NewCPU(bitsCount uint) *CPU {
	cpu := new(CPU)
	cpu.bus = NewBus(bitsCount)
	for i := range cpu.registers {
		cpu.registers[i] = component.NewRegister(bitsCount)
		cpu.bus.AddReceivers(cpu.registers[i])
	}
	cpu.ram = component.NewRAM(bitsCount)
	cpu.bus.AddReceivers(cpu.ram)
	cpu.alu = component.NewALU(bitsCount)
	cpu.bus.AddReceivers(cpu.alu)
	cpu.tmp = component.NewRegister(bitsCount)
	cpu.bus.AddReceivers(cpu.tmp)
	cpu.acc = component.NewRegister(bitsCount)
	cpu.bus.AddReceivers(cpu.acc)
	cpu.instrRegister = component.NewRegister(bitsCount)
	cpu.bus.AddReceivers(cpu.instrRegister)
	cpu.instrAddressRegister = component.NewRegister(bitsCount)
	cpu.bus.AddReceivers(cpu.instrAddressRegister)

	return cpu
}

func (self *CPU) Run() {
	go func() {
		signal := true
		for self.run {
			self.clock <- signal
			signal = !signal
		}
	}()
}

func (self *CPU) Stop() {
	self.run = false
}

func (self *CPU) ReceiveData(data []bool) {
	self.bus.SetInput(data)
}
