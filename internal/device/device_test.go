package device

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
)

type TestDevice struct {
	mock.Mock
}

func (self *TestDevice) ReceiveData(data []bool) {
	args := self.Called(1)
}

func TestIntegration(t *testing.T) {
	cpu := NewCPU()
	console := NewConsole()
	keyboard := NewKeyboard()

	bus := NewBus()
	bus.AddReceivers(cpu, console, keyboard)
	keyboard.SendKey('A')
	assert.Equal(t, "A", console.GetOutput())
}

func TestBus(t *testing.T) {
	bus := NewBus(8)
	testDevice := NewTestDevice(8)
	testDevice.AssertCalled(t, "ReceiveData", mock.Anything)
	bus.AddReceivers(testDevice)
	bus.SendData([]bool{})
	testDevice.AssertNumberOfCalls(t, "ReceiveData", 1)
}
