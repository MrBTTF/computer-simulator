package utils

import (
	"strconv"
	"strings"
)

func CharToBin(ch rune, bitsCount int) []bool {
	result := []bool{}
	for code := int(ch); code > 0; code >>= 1 {
		result = append([]bool{code&1 == 1}, result...)
	}
	result = append(make([]bool, bitsCount-len(result)), result...)
	return result
}

func StringToBin(bin string, bitsCount int) []bool {
	result := []bool{}
	for _, digit := range Reverse(strings.ReplaceAll(bin, " ", "")) {
		result = append(result, digit == '1')
	}
	return result
}

func BinToString(value []bool) string {
	result := ""
	for _, bit := range value {
		if bit {
			result += "1"
		} else {
			result += "0"
		}
	}
	return Reverse(result)
}

func Reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}

func NumToBinString(num int, bitsCount int) string {
	result := strconv.FormatInt(int64(num), 2)
	result = strings.Repeat("0", bitsCount-len(result)) + result
	return result
}

func NumToBin(num int, bitsCount int) []bool {
	var result []bool
	for i := num; i > 0; i >>= 1 {
		if i%2 == 1 {
			result = append(result, true)
		} else {
			result = append(result, false)
		}
	}
	return result
}

func ReplaceRange(s, value string, start, end int) string {
	out := []rune(s)
	out = append(append(out[:start], []rune(value)...), out[end:]...)
	return string(out)
}
