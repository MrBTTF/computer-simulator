package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_CharToBin(t *testing.T) {
	bin := []bool{false, true, false, false, false, false, false, true}
	assert.Equal(t, bin, CharToBin('A', 8))
	bin = []bool{false, false, true, false, false, false, false, true}
	assert.Equal(t, bin, CharToBin('!', 8))
	bin = []bool{false, false, false, false, true, false, false, false, false, true}
	assert.Equal(t, bin, CharToBin('!', 10))
}

func Test_StringToBin(t *testing.T) {
	bin := []bool{false, true, false, false, false, true, false, true}
	assert.Equal(t, bin, StringToBin("01000101", 8))
}
