package component

import (
	// "math/rand"
	"computer-simulator/pkg/utils"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_FullAdder(t *testing.T) {
	testData := []*Data{
		NewData("0", "0", "0", "0", "0"),
		NewData("0", "0", "1", "1", "0"),
		NewData("0", "1", "0", "1", "0"),
		NewData("0", "1", "1", "0", "1"),

		NewData("1", "0", "0", "1", "0"),
		NewData("1", "0", "1", "0", "1"),
		NewData("1", "1", "0", "0", "1"),
		NewData("1", "1", "1", "1", "1"),
	}
	for _, data := range testData {
		input := &ABCPins{
			A: data.NextBool(),
			B: data.NextBool(),
			C: data.NextBool(),
		}

		expected := CarrySumPins{
			Sum:   data.NextBool(),
			Carry: data.NextBool(),
		}

		result := *FullAdder(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_Adder16(t *testing.T) {
	testData := []*Data{
		NewData("0000 0000 0000 0000", "0000 0000 0000 0000", "0",
			"0000 0000 0000 0000", "0"),
		NewData("0000 0000 0000 0001", "0000 0000 0000 0001", "0",
			"0000 0000 0000 0010", "0"),
		NewData("0000 0000 0000 0001", "0000 0000 0000 0001", "1",
			"0000 0000 0000 0011", "0"),
		NewData("1111 1111 1111 1111", "0000 0000 0000 0001", "0",
			"0000 0000 0000 0000", "1"),
		NewData("1111 1111 1111 1111", "0000 0000 0000 0010", "0",
			"0000 0000 0000 0001", "1"),
	}
	for _, data := range testData {
		input := &AdderPins{
			A:       data.NextWord(),
			B:       data.NextWord(),
			CarryIn: data.NextBool(),
		}

		expected := OutCarry{
			Out:      data.NextWord(),
			CarryOut: data.NextBool(),
		}

		result := *Adder16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_Inc16(t *testing.T) {
	num := int(rand.Int31n(int32(wordSize)))
	value := utils.NumToBinString(num, wordSize)
	incValue := utils.NumToBinString(num+1, wordSize)

	testData := []*Data{
		NewData(value,
			incValue),
		NewData("1111 1111 1111 1111",
			"0000 0000 0000 0000"),
	}
	for _, data := range testData {
		input := &In16Pins{
			In: data.NextWord(),
		}

		expected := Out16Pins{
			Out: data.NextWord(),
		}

		result := *Inc16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_LeftShiftN(t *testing.T) {
	testData := []*Data{
		NewData("0000 0000 0000 0000", "0",
			"0000 0000 0000 0000", "0"),
		NewData("0000 0000 0000 1111", "0",
			"0000 0000 0001 1110", "0"),
		NewData("1111 0000 0000 0000", "0",
			"1110 0000 0000 0000", "1"),

		NewData("0000 0000 0000 0000", "1",
			"0000 0000 0000 0001", "0"),
		NewData("0000 0000 0000 1111", "1",
			"0000 0000 0001 1111", "0"),
		NewData("1111 0000 0000 0000", "1",
			"1110 0000 0000 0001", "1"),
	}

	for _, data := range testData {
		input := &ShiftPins{
			In:      data.NextWord(),
			CarryIn: data.NextBool(),
		}

		expected := OutCarry{
			Out:      data.NextWord(),
			CarryOut: data.NextBool(),
		}

		result := *LeftShift16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_RightShiftN(t *testing.T) {
	testData := []*Data{
		NewData("0000 0000 0000 0000", "0",
			"0000 0000 0000 0000", "0"),
		NewData("0000 0000 0001 1110", "0",
			"0000 0000 000 1111", "0"),
		NewData("0000 0000 0000 1111", "0",
			"0000 0000 0000 0111", "1"),

		NewData("0000 0000 0000 0000", "1",
			"1000 0000 0000 0000", "0"),
		NewData("0000 0000 0000 1110", "1",
			"1000 0000 0000 0111", "0"),
		NewData("0000 0000 0000 1111", "1",
			"1000 0000 0000 0111", "1"),
	}

	for _, data := range testData {
		input := &ShiftPins{
			In:      data.NextWord(),
			CarryIn: data.NextBool(),
		}

		expected := OutCarry{
			Out:      data.NextWord(),
			CarryOut: data.NextBool(),
		}

		result := *RightShift16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_ALU(t *testing.T) {
	testData := []*Data{
		NewData("0000 0000 0000 0000", "0000 0000 0000 0000", "0", "000",
			"0000 0000 0000 0000", "0", "0", "1", "1"),
		NewData("0000 0000 0000 0001", "0000 0000 0000 0001", "0", "000",
			"0000 0000 0000 0010", "0", "0", "1", "0"),
		NewData("0000 0000 0000 0001", "0000 0000 0000 0001", "1", "000",
			"0000 0000 0000 0011", "0", "0", "1", "0"),
		NewData("1111 1111 1111 1111", "0000 0000 0000 0001", "0", "000",
			"0000 0000 0000 0000", "1", "1", "0", "1"),
		NewData("1111 1111 1111 1111", "0000 0000 0000 0010", "0", "000",
			"0000 0000 0000 0001", "1", "1", "0", "0"),

		NewData("0000 0000 0001 1110", "1111 1111 1111 1111", "0", "001",
			"0000 0000 000 1111", "0", "0", "0", "0"),
		NewData("0000 0000 0000 1111", "1111 1111 1111 1111", "0", "001",
			"0000 0000 000 0111", "1", "0", "0", "0"),

		NewData("0000 0000 0000 1111", "1111 1111 1111 1111", "0", "010",
			"0000 0000 0001 1110", "0", "0", "0", "0"),
		NewData("1111 0000 0000 0000", "1111 1111 1111 1111", "0", "010",
			"1110 0000 0000 0000", "1", "0", "0", "0"),

		NewData("1111 0000 0000 0000", "1111 1111 1111 1111", "0", "011",
			"0000 1111 1111 1111", "0", "0", "0", "0"),

		NewData("1111 0000 0000 0000", "1111 0000 1111 1111", "0", "100",
			"1111 0000 0000 0000", "0", "0", "0", "0"),

		NewData("1111 0000 0000 0000", "1111 0000 1111 1111", "0", "101",
			"1111 0000 1111 1111", "0", "0", "0", "0"),

		NewData("1111 0000 0000 0000", "1111 0000 1111 1111", "0", "110",
			"0000 0000 1111 1111", "0", "0", "0", "0"),

		NewData("1111 1111 1111 1111", "1111 1111 1111 1111", "0", "111",
			"0000 0000 0000 0000", "0", "0", "1", "1"),
		NewData("1111 1111 1111 1111", "1111 0000 1111 0000", "0", "111",
			"0000 0000 0000 0000", "0", "1", "0", "1"),
		NewData("0000 0000 1111 1111", "1111 0000 1111 0000", "0", "111",
			"0000 0000 0000 0000", "0", "0", "0", "1"),
	}
	for _, data := range testData {
		input := &ALUInputPins{
			A:       data.NextWord(),
			B:       data.NextWord(),
			CarryIn: data.NextBool(),
			Op:      data.Next3Bool(),
		}

		expected := ALUOutputPins{
			Out:      data.NextWord(),
			CarryOut: data.NextBool(),
			ALarger:  data.NextBool(),
			Eq:       data.NextBool(),
			Zero:     data.NextBool(),
		}

		result := *ALU(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}
