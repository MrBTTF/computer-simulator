package component

type CarrySumPins struct {
	Sum   bool
	Carry bool
}

type AdderPins struct {
	A       Word
	B       Word
	CarryIn bool
}

type OutCarry struct {
	Out      Word
	CarryOut bool
}

type ShiftPins struct {
	In      Word
	CarryIn bool
}

type ALUInputPins struct {
	A       Word
	B       Word
	CarryIn bool
	Op      [3]bool
}

type ALUOutputPins struct {
	Out      Word
	CarryOut bool
	ALarger  bool
	Eq       bool
	Zero     bool
}

func make16CarrySum(f func(input *ABCPins) *OutPins, input *ABC16Pins) *Out16Pins {
	var out Out16Pins

	for i := 0; i < wordSize; i++ {
		out.Out[i] = f(&ABCPins{
			input.A[i],
			input.B[i],
			input.C[i],
		}).Out
	}

	return &out
}

func HalfAdder(input *ABPins) *CarrySumPins {
	xor := XOR(input)
	and := AND(input)

	return &CarrySumPins{
		Sum:   xor.Out,
		Carry: and.Out,
	}
}

func FullAdder(input *ABCPins) *CarrySumPins {
	halfAdder1 := HalfAdder(&ABPins{input.A, input.B})
	halfAdder2 := HalfAdder(&ABPins{halfAdder1.Sum, input.C})

	or := OR(&ABPins{halfAdder1.Carry, halfAdder2.Carry})

	return &CarrySumPins{
		Sum:   halfAdder2.Sum,
		Carry: or.Out,
	}
}

func Adder16(input *AdderPins) *OutCarry {
	var out Word
	carry := input.CarryIn
	for i := 0; i < wordSize; i++ {
		carrySum := FullAdder(&ABCPins{input.A[i], input.B[i], carry})
		out[i] = carrySum.Sum
		carry = carrySum.Carry
	}

	return &OutCarry{
		Out:      out,
		CarryOut: carry,
	}
}

func Inc16(input *In16Pins) *Out16Pins {
	var word Word
	word[0] = true
	out := Adder16(&AdderPins{
		A: input.In,
		B: word,
	})
	return &Out16Pins{
		Out: out.Out,
	}
}

func LeftShift16(input *ShiftPins) *OutCarry {
	var out OutCarry

	out.Out[0] = input.CarryIn
	copy(out.Out[1:], input.In[:])
	out.CarryOut = input.In[wordSize-1]

	return &out
}

func RightShift16(input *ShiftPins) *OutCarry {
	var out OutCarry

	out.Out[wordSize-1] = input.CarryIn
	copy(out.Out[:], input.In[1:])
	out.CarryOut = input.In[0]

	return &out
}

func ALU(input *ALUInputPins) *ALUOutputPins {
	add := Adder16(&AdderPins{
		A:       input.A,
		B:       input.B,
		CarryIn: input.CarryIn,
	})

	rsh := RightShift16(&ShiftPins{
		In:      input.A,
		CarryIn: input.CarryIn,
	})

	lsh := LeftShift16(&ShiftPins{
		In:      input.A,
		CarryIn: input.CarryIn,
	})

	not := NOT16(&In16Pins{
		In: input.A,
	})

	and := AND16(&AB16Pins{
		A: input.A,
		B: input.B,
	})

	or := OR16(&AB16Pins{
		A: input.A,
		B: input.B,
	})

	comp := Comparator16(&AB16Pins{
		A: input.A,
		B: input.B,
	})

	mux := MUX8Way16(&Mux8Way16InputPins{
		A:   add.Out,
		B:   rsh.Out,
		C:   lsh.Out,
		D:   not.Out,
		E:   and.Out,
		F:   or.Out,
		G:   comp.Out,
		Sel: input.Op,
	})

	muxCarry := MUX4Way(&Mux4WayInputPins{
		A:   add.CarryOut,
		B:   rsh.CarryOut,
		C:   lsh.CarryOut,
		D:   false,
		Sel: [2]bool{input.Op[0], input.Op[1]},
	})

	muxCarry2 := MUX(&MuxInputPins{
		A:   muxCarry.Out,
		B:   false,
		Sel: input.Op[2],
	})

	or16Way := OR16Way(&In16Pins{
		In: mux.Out,
	})

	notZero := NOT(&InPins{
		In: or16Way.Out,
	})

	return &ALUOutputPins{
		Out:      mux.Out,
		CarryOut: muxCarry2.Out,
		ALarger:  comp.ALarger,
		Eq:       comp.Eq,
		Zero:     notZero.Out,
	}
}
