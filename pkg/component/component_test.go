package component

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_Connection(t *testing.T) {

	input := &ABPins{true, true}
	output := NAND(input)
	expected := &OutPins{false}

	assert.Equal(t, expected, output, "output: %v\ninput: %v", output, input)

	input = &ABPins{output.Out, false}
	output = AND(input)
	expected = &OutPins{false}

	assert.Equal(t, expected, output, "output: %v\ninput: %v", output, input)

}
