package component

import (
	"computer-simulator/pkg/utils"
	"math/rand"
	"strings"
)

type Data struct {
	data    [][]bool
	storage map[string]interface{}
	curr    int
}

func NewData(data ...string) *Data {
	d := new(Data)
	d.storage = make(map[string]interface{})
	for _, s := range data {
		s = strings.ReplaceAll(s, " ", "")
		d.data = append(d.data, utils.StringToBin(s, len(s)))
	}
	return d
}

func (d *Data) WithKeyValue(k string, v interface{}) *Data {
	d.storage[k] = v
	return d
}

func (d *Data) NextBool() bool {
	d.curr++
	return d.data[d.curr-1][0]
}

func (d *Data) Next2Bool() [2]bool {
	var result [2]bool
	copy(result[:], d.data[d.curr])
	d.curr++
	return result
}

func (d *Data) Next3Bool() [3]bool {
	var result [3]bool
	copy(result[:], d.data[d.curr])
	d.curr++
	return result
}

func (d *Data) Next256Bool() [256]bool {
	var result [256]bool
	copy(result[:], d.data[d.curr])
	d.curr++
	return result
}

func (d *Data) NextByte() Byte {
	var result Byte
	copy(result[:], d.data[d.curr])
	d.curr++
	return result
}

func (d *Data) NextWord() Word {
	var result Word
	copy(result[:], d.data[d.curr])
	d.curr++
	return result
}

func (d *Data) NextAddress() Address {
	var result Address
	copy(result[:], d.data[d.curr])
	d.curr++
	return result
}

func (d *Data) GetIntValue(key string) int {
	return d.storage[key].(int)
}

func (d *Data) GetByteValue(key string) Byte {
	return d.storage[key].(Byte)
}

func (d *Data) GetMemoryValue(key string) Memory {
	return d.storage[key].(Memory)
}

func wordWithOneRandBit() string {
	result := []rune(strings.Repeat("0", wordSize))
	bit := int(rand.Int31n(wordSize - 1))
	result[bit] = '1'
	return string(result)
}

func randomWord() string {
	word := int(rand.Int31n(ram16KSize - 1))
	return utils.NumToBinString(word, wordSize)
}
