package component

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_NAND(t *testing.T) {
	testData := []*Data{
		NewData("0", "0", "1"),
		NewData("1", "0", "1"),
		NewData("0", "1", "1"),
		NewData("1", "1", "0"),
	}
	for _, data := range testData {
		var input ABPins
		input.A = data.NextBool()
		input.B = data.NextBool()

		var expected OutPins
		expected.Out = data.NextBool()

		result := *NAND(&input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_NOT(t *testing.T) {
	testData := []*Data{
		NewData("0", "1"),
		NewData("1", "0"),
	}
	for _, data := range testData {
		var input InPins
		input.In = data.NextBool()

		var expected OutPins
		expected.Out = data.NextBool()

		result := *NOT(&input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_NOT16(t *testing.T) {
	testData := []*Data{
		NewData("0000 0000 0000 0000",
			"1111 1111 1111 1111"),
		NewData("1111 1111 1111 1111",
			"0000 0000 0000 0000"),
	}
	for _, data := range testData {
		input := &In16Pins{
			In: data.NextWord(),
		}

		expected := Out16Pins{
			Out: data.NextWord(),
		}

		result := *NOT16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_AND(t *testing.T) {
	testData := []*Data{
		NewData("0", "0", "0"),
		NewData("1", "0", "0"),
		NewData("0", "1", "0"),
		NewData("1", "1", "1"),
	}
	for _, data := range testData {
		input := &ABPins{
			A: data.NextBool(),
			B: data.NextBool(),
		}

		expected := OutPins{
			Out: data.NextBool(),
		}

		result := *AND(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_AND16(t *testing.T) {
	randomWord := wordWithOneRandBit()
	testData := []*Data{
		NewData("0000 0000 0000 0000", "0000 0000 0000 0000",
			"0000 0000 0000 0000"),
		NewData("0000 0000 0000 0000", randomWord,
			"0000 0000 0000 0000"),
		NewData(randomWord, randomWord,
			randomWord),
		NewData("1111 1111 1111 1111", "1111 1111 1111 1111",
			"1111 1111 1111 1111"),
	}
	for _, data := range testData {
		input := &AB16Pins{
			A: data.NextWord(),
			B: data.NextWord(),
		}

		expected := Out16Pins{
			Out: data.NextWord(),
		}

		result := *AND16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_OR(t *testing.T) {
	testData := []*Data{
		NewData("0", "0", "0"),
		NewData("1", "0", "1"),
		NewData("0", "1", "1"),
		NewData("1", "1", "1"),
	}
	for _, data := range testData {
		input := &ABPins{
			A: data.NextBool(),
			B: data.NextBool(),
		}

		expected := OutPins{
			Out: data.NextBool(),
		}

		result := *OR(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_XOR(t *testing.T) {
	testData := []*Data{
		NewData("0", "0", "0"),
		NewData("1", "0", "1"),
		NewData("0", "1", "1"),
		NewData("1", "1", "0"),
	}
	for _, data := range testData {
		input := &ABPins{
			A: data.NextBool(),
			B: data.NextBool(),
		}

		expected := OutPins{
			Out: data.NextBool(),
		}

		result := *XOR(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_OR16Way(t *testing.T) {
	randomWord := wordWithOneRandBit()
	testData := []*Data{
		NewData("0000 0000 0000 0000",
			"0"),
		NewData(randomWord,
			"1"),
		NewData("1111 1111 1111 1111",
			"1"),
	}
	for _, data := range testData {
		input := &In16Pins{
			In: data.NextWord(),
		}

		expected := OutPins{
			Out: data.NextBool(),
		}

		result := *OR16Way(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_Compartor16(t *testing.T) {
	testData := []*Data{
		NewData("0000 0000 0000 0000", "0000 0000 0000 0000",
			"0000 0000 0000 0000", "0", "1"),
		NewData("1111 1111 1111 1111", "1111 1111 1111 1111",
			"0000 0000 0000 0000", "0", "1"),

		NewData("0000 0000 0000 0001", "0000 0000 0000 0000",
			"0000 0000 0000 0001", "1", "0"),
		NewData("0000 0000 0000 0000", "0000 0000 0000 0001",
			"0000 0000 0000 0001", "0", "0"),
		NewData("1111 1111 1111 1111", "0000 0000 0000 0000",
			"1111 1111 1111 1111", "1", "0"),
	}
	for _, data := range testData {
		input := &AB16Pins{
			A: data.NextWord(),
			B: data.NextWord(),
		}

		expected := ComparatorOutPins{
			Out:     data.NextWord(),
			ALarger: data.NextBool(),
			Eq:      data.NextBool(),
		}

		result := *Comparator16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}
