package component

import (
	"computer-simulator/pkg/utils"
)

const (
	byteSize   = 8
	wordSize   = 16
	ram16KSize = 16384
	adressSize = 14
)

var (
	AllZeroWord Word
	AllOneWord  = Word{
		true, true, true, true,
		true, true, true, true,
		true, true, true, true,
		true, true, true, true,
	}
)

type Word [wordSize]bool

func NewWord(bin []bool) Word {
	return *(*[wordSize]bool)(bin)
}

func WordFromInt(i int) Word {
	return *(*[wordSize]bool)(utils.NumToBin(i, wordSize))
}

func WordFromString(s string) Word {
	return *(*[wordSize]bool)(utils.StringToBin(s, wordSize))
}

func (w Word) String() string {
	return utils.BinToString(w[:])
}

func (w Word) FirstByte() Byte {
	return *(*[byteSize]bool)(w[:wordSize/2])
}

func (w Word) SecondByte() Byte {
	return *(*[byteSize]bool)(w[wordSize/2:])
}

type Byte [byteSize]bool

func NewByte(bin []bool) Byte {
	return *(*[byteSize]bool)(bin)
}

func ByteFromInt(i int) Byte {
	return *(*[byteSize]bool)(utils.NumToBin(i, wordSize))
}

func (b Byte) String() string {
	return utils.BinToString(b[:])
}

func (b Byte) Word() Word {
	var w Word
	copy(w[:], b[:])
	return w
}

type ABPins struct {
	A bool
	B bool
}

func make16AB(f func(input *ABPins) *OutPins, input *AB16Pins) *Out16Pins {
	var out Out16Pins

	for i := 0; i < wordSize; i++ {
		out.Out[i] = f(&ABPins{
			input.A[i],
			input.B[i],
		}).Out
	}

	return &out
}

type ABCPins struct {
	A bool
	B bool
	C bool
}

type ABC16Pins struct {
	A Word
	B Word
	C Word
}

type AB16Pins struct {
	A Word
	B Word
}

type InPins struct {
	In bool
}

type In16Pins struct {
	In Word
}

type OutPins struct {
	Out bool
}

type Out8Pins struct {
	Out Byte
}

type Out16Pins struct {
	Out Word
}
