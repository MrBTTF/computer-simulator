package component

type FSMOutPins struct {
	State0 bool
	State1 bool
	State2 bool
	State3 bool
	State4 bool
	State5 bool
	Out    Word
}

type FetchExecuteInPins struct {
	ClockIn  bool
	FSMReset bool
	BusOut   Word
	FSMOut   Word
	PCOut    Word
	IROut    Word
	RAMOut   Memory
}

type FetchExecuteOutPins struct {
	BusOut Word
	FSMOut Word
	PCOut  Word
	IROut  Word
}

type ValueLogicInPins struct {
	ClockIn     bool
	Control     bool
	State1      bool
	State2      bool
	State5      bool
	FSMOut      Word
	IROut       Word
	ValueBitOut bool
}

type ValueLogicOutPins struct {
	ValueBitOut bool
	FSMReset    bool
}

type ReadRegisterInPins struct {
	ClockIn bool
	Control bool
	Address [2]bool
	R0Out   Word
	R1Out   Word
	R2Out   Word
	R3Out   Word
	BusOut  Word
}

type WriteRegisterOutPins struct {
	R0Out Word
	R1Out Word
	R2Out Word
	R3Out Word
}

type MovRxRyInPins struct {
	ClockIn    bool
	Control    bool
	State2     bool
	State3     bool
	IROut      Word
	BusOut     Word
	R0Out      Word
	R1Out      Word
	R2Out      Word
	R3Out      Word
	AddressOut Word
	RAMOut     Memory
}

type MovRxRyOutPins struct {
	R0Out      Word
	R1Out      Word
	R2Out      Word
	R3Out      Word
	BusOut     Word
	AddressOut Word
	RAMOut     Memory
}

type MovRxValueInPins struct {
	ClockIn     bool
	Control     bool
	State4      bool
	State5      bool
	IROut       Word
	BusOut      Word
	R0Out       Word
	R1Out       Word
	R2Out       Word
	R3Out       Word
	ValueBitOut bool
}

type MovRxValueOutPins struct {
	R0Out       Word
	R1Out       Word
	R2Out       Word
	R3Out       Word
	BusOut      Word
	IROut       Word
	ValueBitOut bool
}

type FlagsPins struct {
	CarryOut bool
	ALarger  bool
	Eq       bool
	Zero     bool
}

type SendToALUInPins struct {
	ClockIn bool
	Control bool
	State2  bool
	State3  bool
	State4  bool
	CarryIn bool
	R0Out   Word
	R1Out   Word
	R2Out   Word
	R3Out   Word
	IROut   Word
	BusOut  Word
	TMPOut  Word
	ACCOut  Word
}

type SendToALUOutPins struct {
	Flags  FlagsPins
	R0Out  Word
	R1Out  Word
	R2Out  Word
	R3Out  Word
	BusOut Word
	TMPOut Word
	ACCOut Word
}

type BusInputPins struct {
	In  Word
	Out Word
}

func BUS(input *BusInputPins) *Out16Pins {
	return OR16(&AB16Pins{
		A: input.In,
		B: input.Out,
	})
}

func FSM(input *PCInputPins) *FSMOutPins {
	fsm := PC(input)

	var sel [3]bool
	copy(sel[:], fsm.Out[:])
	dmux := DMUX8Way(&DMux8WayInputPins{
		In:  true,
		Sel: sel,
	})

	return &FSMOutPins{
		State0: dmux.A,
		State1: dmux.B,
		State2: dmux.C,
		State3: dmux.D,
		State4: dmux.E,
		State5: dmux.F,
		Out:    fsm.NextOut,
	}

}

func FetchExecute(input *FetchExecuteInPins) *FetchExecuteOutPins {
	and02 := AND(&ABPins{
		A: input.FSMOut[0],
		B: input.FSMOut[2],
	}).Out

	state4 := AND(&ABPins{
		A: NOT(&InPins{input.FSMOut[1]}).Out,
		B: and02,
	}).Out

	fsmReset := OR(&ABPins{
		A: state4,
		B: input.FSMReset,
	}).Out

	pcIn := Word{}

	fsm := FSM(&PCInputPins{
		ClockIn: input.ClockIn,
		Inc:     true,
		Reset:   fsmReset,
		Out:     input.FSMOut,
	})

	state01 := OR(&ABPins{fsm.State0, fsm.State1}).Out

	pc := PC(&PCInputPins{
		In:      pcIn,
		ClockIn: input.ClockIn,
		Inc:     state01,
		Out:     input.PCOut,
	})

	ram := RAM16K(&RAM16KInputPins{
		ClockIn: input.ClockIn,
		Address: AddressFromWord(pc.Out),
		Enable:  state01,
		Out:     input.RAMOut,
	})

	bus := BUS(&BusInputPins{
		In:  ram.Out.Word(),
		Out: input.BusOut,
	})

	ir := REG2Bytes(&Reg2BytesInputPins{
		In:      bus.Out.FirstByte(),
		ClockIn: input.ClockIn,
		Enable:  true,
		Load0:   fsm.State0,
		Load1:   fsm.State1,
		Out:     input.IROut,
	})

	var out FetchExecuteOutPins
	out.BusOut = bus.Out
	out.FSMOut = fsm.Out
	out.PCOut = pc.NextOut
	out.IROut = ir.NextOut

	return &out
}

func ValueLogic(input *ValueLogicInPins) *ValueLogicOutPins {
	and1 := AND(&ABPins{
		A: input.State1,
		B: input.Control,
	})

	orLoad := OR(&ABPins{
		A: and1.Out,
		B: input.State5,
	})

	valueBit := BIT(
		&BitInputPins{
			In:      and1.Out,
			ClockIn: input.ClockIn,
			Load:    orLoad.Out,
			Enable:  true,
			Out:     input.ValueBitOut,
		},
	)

	fsmReset := AND(&ABPins{
		A: and1.Out,
		B: NOT(&InPins{valueBit.Out}).Out,
	})

	control := OR(&ABPins{
		A: input.Control,
		B: valueBit.Out,
	})

	if control.Out {
		return &ValueLogicOutPins{
			FSMReset:    fsmReset.Out,
			ValueBitOut: valueBit.NextOut,
		}
	}
	return nil
}

func ReadRegister(input *ReadRegisterInPins) (busOut *Word) {
	mux := DMUX4Way(&DMux4WayInputPins{
		In:  input.Control,
		Sel: input.Address,
	})

	r0 := REG16(&Reg16InputPins{
		ClockIn: input.ClockIn,
		Enable:  mux.A,
		Out:     input.R0Out,
	})

	r1 := REG16(&Reg16InputPins{
		ClockIn: input.ClockIn,
		Enable:  mux.B,
		Out:     input.R1Out,
	})

	r2 := REG16(&Reg16InputPins{
		ClockIn: input.ClockIn,
		Enable:  mux.C,
		Out:     input.R2Out,
	})

	r3 := REG16(&Reg16InputPins{
		ClockIn: input.ClockIn,
		Enable:  mux.D,
		Out:     input.R3Out,
	})

	bus := BUS(&BusInputPins{
		In:  r0.Out,
		Out: input.BusOut,
	})
	bus = BUS(&BusInputPins{
		In:  r1.Out,
		Out: bus.Out,
	})
	bus = BUS(&BusInputPins{
		In:  r2.Out,
		Out: bus.Out,
	})
	bus = BUS(&BusInputPins{
		In:  r3.Out,
		Out: bus.Out,
	})

	return &bus.Out
}

func WriteRegister(input *ReadRegisterInPins) *WriteRegisterOutPins {
	mux := DMUX4Way(&DMux4WayInputPins{
		In:  input.Control,
		Sel: input.Address,
	})

	r0 := REG16(&Reg16InputPins{
		In:      input.BusOut,
		ClockIn: input.ClockIn,
		Load:    mux.A,
		Out:     input.R0Out,
	})

	r1 := REG16(&Reg16InputPins{
		In:      input.BusOut,
		ClockIn: input.ClockIn,
		Load:    mux.B,
		Out:     input.R1Out,
	})

	r2 := REG16(&Reg16InputPins{
		In:      input.BusOut,
		ClockIn: input.ClockIn,
		Load:    mux.C,
		Out:     input.R2Out,
	})

	r3 := REG16(&Reg16InputPins{
		In:      input.BusOut,
		ClockIn: input.ClockIn,
		Load:    mux.D,
		Out:     input.R3Out,
	})

	return &WriteRegisterOutPins{
		R0Out: r0.NextOut,
		R1Out: r1.NextOut,
		R2Out: r2.NextOut,
		R3Out: r3.NextOut,
	}
}

func MovRxRy(input *MovRxRyInPins) *MovRxRyOutPins {
	var selRead, selWrite [2]bool
	copy(selRead[:], input.IROut[:2])
	copy(selWrite[:], input.IROut[2:4])

	orState23 := OR(&ABPins{
		A: input.State2,
		B: input.State3,
	})

	and1 := AND(&ABPins{
		A: orState23.Out,
		B: input.Control,
	})

	registerOut := ReadRegister(&ReadRegisterInPins{
		ClockIn: input.ClockIn,
		Control: and1.Out,
		Address: selRead,
		R0Out:   input.R0Out,
		R1Out:   input.R1Out,
		R2Out:   input.R2Out,
		R3Out:   input.R3Out,
		BusOut:  input.BusOut,
	})

	and2 := AND(&ABPins{
		A: input.State3,
		B: input.Control,
	})

	writeRegister := WriteRegister(&ReadRegisterInPins{
		ClockIn: input.ClockIn,
		Control: and2.Out,
		Address: selWrite,
		R0Out:   input.R0Out,
		R1Out:   input.R1Out,
		R2Out:   input.R2Out,
		R3Out:   input.R3Out,
		BusOut:  *registerOut,
	})

	if input.Control {
		return &MovRxRyOutPins{
			R0Out:  writeRegister.R0Out,
			R1Out:  writeRegister.R1Out,
			R2Out:  writeRegister.R2Out,
			R3Out:  writeRegister.R3Out,
			BusOut: *registerOut,
			RAMOut: input.RAMOut,
		}
	}

	return nil
}

func MovRxRefRy(input *MovRxRyInPins) *MovRxRyOutPins {
	var selRead, selWrite [2]bool
	copy(selRead[:], input.IROut[:2])
	copy(selWrite[:], input.IROut[2:4])

	and1 := AND(&ABPins{
		A: input.State2,
		B: input.Control,
	})

	and2 := AND(&ABPins{
		A: input.State3,
		B: input.Control,
	})

	registerOut := ReadRegister(&ReadRegisterInPins{
		ClockIn: input.ClockIn,
		Control: and1.Out,
		Address: selRead,
		R0Out:   input.R0Out,
		R1Out:   input.R1Out,
		R2Out:   input.R2Out,
		R3Out:   input.R3Out,
		BusOut:  input.BusOut,
	})

	addressReg := REG16(&Reg16InputPins{
		*registerOut,
		input.ClockIn,
		and1.Out,
		and2.Out,
		input.AddressOut,
	})

	ram := RAM16K(&RAM16KInputPins{
		ClockIn: input.ClockIn,
		Address: AddressFromWord(addressReg.Out),
		Enable:  and2.Out,
		Out:     input.RAMOut,
	})

	bus := BUS(&BusInputPins{
		In:  ram.Out.Word(),
		Out: input.BusOut,
	})

	writeRegister := WriteRegister(&ReadRegisterInPins{
		ClockIn: input.ClockIn,
		Control: and2.Out,
		Address: selWrite,
		R0Out:   input.R0Out,
		R1Out:   input.R1Out,
		R2Out:   input.R2Out,
		R3Out:   input.R3Out,
		BusOut:  bus.Out,
	})

	if input.Control {
		return &MovRxRyOutPins{
			R0Out:      writeRegister.R0Out,
			R1Out:      writeRegister.R1Out,
			R2Out:      writeRegister.R2Out,
			R3Out:      writeRegister.R3Out,
			BusOut:     bus.Out,
			RAMOut:     input.RAMOut,
			AddressOut: addressReg.NextOut,
		}
	}
	return nil
}

func MovRefRxRy(input *MovRxRyInPins) *MovRxRyOutPins {

	var selRead, selRef [2]bool
	copy(selRead[:], input.IROut[:2])
	copy(selRef[:], input.IROut[2:4])

	and1 := AND(&ABPins{
		A: input.State2,
		B: input.Control,
	})

	and2 := AND(&ABPins{
		A: input.State3,
		B: input.Control,
	})

	registerRefOut := ReadRegister(&ReadRegisterInPins{
		ClockIn: input.ClockIn,
		Control: and1.Out,
		Address: selRef,
		R0Out:   input.R0Out,
		R1Out:   input.R1Out,
		R2Out:   input.R2Out,
		R3Out:   input.R3Out,
		BusOut:  input.BusOut,
	})
	addressReg := REG16(&Reg16InputPins{
		*registerRefOut,
		input.ClockIn,
		and1.Out,
		and2.Out,
		input.AddressOut,
	})

	registerOut := ReadRegister(&ReadRegisterInPins{
		ClockIn: input.ClockIn,
		Control: and2.Out,
		Address: selRead,
		R0Out:   input.R0Out,
		R1Out:   input.R1Out,
		R2Out:   input.R2Out,
		R3Out:   input.R3Out,
		BusOut:  input.BusOut,
	})

	ram := RAM16K(&RAM16KInputPins{
		In:      registerOut.FirstByte(),
		Load:    and2.Out,
		ClockIn: input.ClockIn,
		Address: AddressFromWord(input.AddressOut),
		Out:     input.RAMOut,
	})

	bus := BUS(&BusInputPins{
		In:  *registerRefOut,
		Out: input.BusOut,
	})

	bus = BUS(&BusInputPins{
		In:  *registerOut,
		Out: bus.Out,
	})

	if input.Control {
		return &MovRxRyOutPins{
			R0Out:      input.R0Out,
			R1Out:      input.R1Out,
			R2Out:      input.R2Out,
			R3Out:      input.R3Out,
			BusOut:     bus.Out,
			RAMOut:     ram.MemoryOut,
			AddressOut: addressReg.NextOut,
		}
	}
	return nil
}

func MovRxValue(input *MovRxValueInPins) *MovRxValueOutPins {
	var selWrite [2]bool
	copy(selWrite[:], input.IROut[2:4])

	valueBit := BIT(
		&BitInputPins{
			In:      NOT(&InPins{input.ValueBitOut}).Out,
			ClockIn: input.ClockIn,
			Load:    input.State5,
			Enable:  true,
			Out:     input.ValueBitOut,
		},
	)

	andEnable := AND(&ABPins{
		A: valueBit.Out,
		B: input.State5,
	})

	ir := REG2Bytes(&Reg2BytesInputPins{
		In:      input.IROut.FirstByte(),
		ClockIn: input.ClockIn,
		Enable:  andEnable.Out,
		Load0:   false,
		Load1:   false,
		Out:     input.IROut,
	})

	bus := BUS(&BusInputPins{
		In:  ir.NextOut,
		Out: input.BusOut,
	})

	writeRegister := WriteRegister(&ReadRegisterInPins{
		ClockIn: input.ClockIn,
		Control: andEnable.Out,
		Address: selWrite,
		R0Out:   input.R0Out,
		R1Out:   input.R1Out,
		R2Out:   input.R2Out,
		R3Out:   input.R3Out,
		BusOut:  bus.Out,
	})

	or := OR(&ABPins{
		A: input.State4,
		B: input.State5,
	})

	control := AND(&ABPins{
		A: or.Out,
		B: valueBit.Out,
	})

	if control.Out {
		return &MovRxValueOutPins{
			R0Out:       writeRegister.R0Out,
			R1Out:       writeRegister.R1Out,
			R2Out:       writeRegister.R2Out,
			R3Out:       writeRegister.R3Out,
			BusOut:      bus.Out,
			IROut:       ir.NextOut,
			ValueBitOut: valueBit.NextOut,
		}
	}

	return nil
}

func SendToALU(input *SendToALUInPins) *SendToALUOutPins {
	var selA, selB [2]bool
	copy(selA[:], input.IROut[:2])
	copy(selB[:], input.IROut[2:4])

	loadTmp := AND(&ABPins{
		A: input.State2,
		B: input.Control,
	})

	orState23 := OR(&ABPins{
		A: input.State2,
		B: input.State3,
	})

	enableTmp := AND(&ABPins{
		A: orState23.Out,
		B: input.Control,
	})

	registerA := ReadRegister(&ReadRegisterInPins{
		ClockIn: input.ClockIn,
		Control: loadTmp.Out,
		Address: selA,
		R0Out:   input.R0Out,
		R1Out:   input.R1Out,
		R2Out:   input.R2Out,
		R3Out:   input.R3Out,
		BusOut:  input.BusOut,
	})

	bus := BUS(&BusInputPins{
		In:  *registerA,
		Out: input.BusOut,
	})

	tmp := REG16(&Reg16InputPins{
		In:      bus.Out,
		ClockIn: input.ClockIn,
		Load:    loadTmp.Out,
		Enable:  enableTmp.Out,
		Out:     input.TMPOut,
	})

	loadAcc := AND(&ABPins{
		A: input.State3,
		B: input.Control,
	})

	registerB := ReadRegister(&ReadRegisterInPins{
		ClockIn: input.ClockIn,
		Control: loadAcc.Out,
		Address: selB,
		R0Out:   input.R0Out,
		R1Out:   input.R1Out,
		R2Out:   input.R2Out,
		R3Out:   input.R3Out,
		BusOut:  input.BusOut,
	})

	bus = BUS(&BusInputPins{
		In:  *registerB,
		Out: bus.Out,
	})

	var op [3]bool
	copy(op[:], input.IROut[4:8])

	alu := ALU(&ALUInputPins{
		A:       tmp.Out,
		B:       bus.Out,
		CarryIn: input.CarryIn,
		Op:      op,
	})

	flags := FlagsPins{
		CarryOut: alu.CarryOut,
		ALarger:  alu.ALarger,
		Eq:       alu.Eq,
		Zero:     alu.Zero,
	}

	enableAcc := AND(&ABPins{
		A: input.State4,
		B: input.Control,
	})

	acc := REG16(&Reg16InputPins{
		In:      alu.Out,
		ClockIn: input.ClockIn,
		Load:    loadAcc.Out,
		Enable:  enableAcc.Out,
		Out:     input.ACCOut,
	})

	bus = BUS(&BusInputPins{
		In:  acc.Out,
		Out: bus.Out,
	})

	writeRegister := WriteRegister(&ReadRegisterInPins{
		ClockIn: input.ClockIn,
		Control: enableAcc.Out,
		Address: selB,
		R0Out:   input.R0Out,
		R1Out:   input.R1Out,
		R2Out:   input.R2Out,
		R3Out:   input.R3Out,
		BusOut:  bus.Out,
	})

	if input.Control {
		return &SendToALUOutPins{
			BusOut: bus.Out,
			Flags:  flags,
			R0Out:  writeRegister.R0Out,
			R1Out:  writeRegister.R1Out,
			R2Out:  writeRegister.R2Out,
			R3Out:  writeRegister.R3Out,
			TMPOut: tmp.NextOut,
			ACCOut: acc.NextOut,
		}
	}
	return nil
}

type CPU struct {
	busOut      Word
	fsmOut      Word
	fsmReset    bool
	pcOut       Word
	irOut       Word
	addressOut  Word
	ramOut      Memory
	r0Out       Word
	r1Out       Word
	r2Out       Word
	r3Out       Word
	tmpOut      Word
	accOut      Word
	flagsOut    Word
	valueBitOut bool
}

func NewCPU(r0Out, r1Out, r2Out, r3Out Word, memory Memory) *CPU {
	return &CPU{
		ramOut: memory,
		r0Out:  r0Out,
		r1Out:  r1Out,
		r2Out:  r2Out,
		r3Out:  r3Out,
	}
}

func (cpu *CPU) step(clockIn bool) {
	cpu.busOut = Word{}
	fsm := FSM(&PCInputPins{
		ClockIn: clockIn,
		Inc:     true,
		Out:     cpu.fsmOut,
	})

	fetchExecute := FetchExecute(&FetchExecuteInPins{
		ClockIn:  clockIn,
		FSMReset: cpu.fsmReset,
		BusOut:   cpu.busOut,
		FSMOut:   cpu.fsmOut,
		IROut:    cpu.irOut,
		PCOut:    cpu.pcOut,
		RAMOut:   cpu.ramOut,
	})
	if fetchExecute != nil {
		cpu.fsmOut = fetchExecute.FSMOut
		cpu.busOut = fetchExecute.BusOut
		cpu.irOut = fetchExecute.IROut
		cpu.pcOut = fetchExecute.PCOut
	}

	var opcode Byte
	copy(opcode[:], cpu.irOut[4:4+byteSize])
	decoder := DMUX256Way(&DMux256WayInputPins{
		In:  true,
		Sel: opcode,
	})

	valueLogic := ValueLogic(&ValueLogicInPins{
		ClockIn:     clockIn,
		Control:     opcode[2],
		State1:      fsm.State1,
		State2:      fsm.State2,
		State5:      fsm.State5,
		FSMOut:      cpu.fsmOut,
		IROut:       cpu.irOut,
		ValueBitOut: cpu.valueBitOut,
	})

	if valueLogic != nil {
		cpu.fsmReset = valueLogic.FSMReset
		cpu.valueBitOut = valueLogic.ValueBitOut
	}

	movRxRy := MovRxRy(&MovRxRyInPins{
		ClockIn: clockIn,
		Control: decoder.Out[0],
		State2:  fsm.State2,
		State3:  fsm.State3,
		BusOut:  cpu.busOut,
		IROut:   cpu.irOut,
		RAMOut:  cpu.ramOut,
		R0Out:   cpu.r0Out,
		R1Out:   cpu.r1Out,
		R2Out:   cpu.r2Out,
		R3Out:   cpu.r3Out,
	})

	if movRxRy != nil {
		cpu.r0Out = movRxRy.R0Out
		cpu.r1Out = movRxRy.R1Out
		cpu.r2Out = movRxRy.R2Out
		cpu.r3Out = movRxRy.R3Out
		cpu.busOut = movRxRy.BusOut
		cpu.ramOut = movRxRy.RAMOut
	}

	movRxRefRy := MovRxRefRy(&MovRxRyInPins{
		ClockIn:    clockIn,
		Control:    decoder.Out[1],
		State2:     fsm.State2,
		State3:     fsm.State3,
		BusOut:     cpu.busOut,
		IROut:      cpu.irOut,
		RAMOut:     cpu.ramOut,
		AddressOut: cpu.addressOut,
		R0Out:      cpu.r0Out,
		R1Out:      cpu.r1Out,
		R2Out:      cpu.r2Out,
		R3Out:      cpu.r3Out,
	})

	if movRxRefRy != nil {
		cpu.r0Out = movRxRefRy.R0Out
		cpu.r1Out = movRxRefRy.R1Out
		cpu.r2Out = movRxRefRy.R2Out
		cpu.r3Out = movRxRefRy.R3Out
		cpu.busOut = movRxRefRy.BusOut
		cpu.ramOut = movRxRefRy.RAMOut
		cpu.addressOut = movRxRefRy.AddressOut
	}

	movRefRxRy := MovRefRxRy(&MovRxRyInPins{
		ClockIn:    clockIn,
		Control:    decoder.Out[2],
		State2:     fsm.State2,
		State3:     fsm.State3,
		BusOut:     cpu.busOut,
		IROut:      cpu.irOut,
		RAMOut:     cpu.ramOut,
		AddressOut: cpu.addressOut,
		R0Out:      cpu.r0Out,
		R1Out:      cpu.r1Out,
		R2Out:      cpu.r2Out,
		R3Out:      cpu.r3Out,
	})

	if movRefRxRy != nil {
		cpu.r0Out = movRefRxRy.R0Out
		cpu.r1Out = movRefRxRy.R1Out
		cpu.r2Out = movRefRxRy.R2Out
		cpu.r3Out = movRefRxRy.R3Out
		cpu.busOut = movRefRxRy.BusOut
		cpu.ramOut = movRefRxRy.RAMOut
		cpu.addressOut = movRefRxRy.AddressOut
	}

	movRxValue := MovRxValue(&MovRxValueInPins{
		ClockIn:     clockIn,
		Control:     opcode[2],
		State4:      fsm.State4,
		State5:      fsm.State5,
		BusOut:      cpu.busOut,
		IROut:       cpu.irOut,
		R0Out:       cpu.r0Out,
		R1Out:       cpu.r1Out,
		R2Out:       cpu.r2Out,
		R3Out:       cpu.r3Out,
		ValueBitOut: cpu.valueBitOut,
	})

	if movRxValue != nil {
		cpu.r0Out = movRxValue.R0Out
		cpu.r1Out = movRxValue.R1Out
		cpu.r2Out = movRxValue.R2Out
		cpu.r3Out = movRxValue.R3Out
		cpu.busOut = movRxValue.BusOut
		cpu.irOut = movRxValue.IROut
	}

	sendToALU := SendToALU(&SendToALUInPins{
		ClockIn: clockIn,
		Control: opcode[3],
		State2:  fsm.State2,
		State3:  fsm.State3,
		State4:  fsm.State4,
		CarryIn: false,
		R0Out:   cpu.r0Out,
		R1Out:   cpu.r1Out,
		R2Out:   cpu.r2Out,
		R3Out:   cpu.r3Out,
		BusOut:  cpu.busOut,
		IROut:   cpu.irOut,
		TMPOut:  cpu.tmpOut,
		ACCOut:  cpu.accOut,
	})

	if sendToALU != nil {
		cpu.r0Out = sendToALU.R0Out
		cpu.r1Out = sendToALU.R1Out
		cpu.r2Out = sendToALU.R2Out
		cpu.r3Out = sendToALU.R3Out
		cpu.busOut = sendToALU.BusOut
		cpu.accOut = sendToALU.ACCOut
		cpu.tmpOut = sendToALU.TMPOut
	}

}
