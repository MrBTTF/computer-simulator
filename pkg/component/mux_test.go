package component

import (
	"computer-simulator/pkg/utils"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_MUX(t *testing.T) {
	testData := []*Data{
		NewData("0", "0", "0",
			"0"),
		NewData("1", "0", "0",
			"1"),
		NewData("0", "1", "0",
			"0"),
		NewData("1", "1", "0",
			"1"),

		NewData("0", "0", "1",
			"0"),
		NewData("1", "0", "1",
			"0"),
		NewData("0", "1", "1",
			"1"),
		NewData("1", "1", "1",
			"1"),
	}
	for _, data := range testData {
		input := &MuxInputPins{
			A:   data.NextBool(),
			B:   data.NextBool(),
			Sel: data.NextBool(),
		}

		expected := OutPins{
			Out: data.NextBool(),
		}

		result := *MUX(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_MUX16(t *testing.T) {
	testData := []*Data{
		NewData("0000 0000 0000 0000", "1111 1111 1111 1111", "0",
			"0000 0000 0000 0000"),
		NewData("0000 0000 0000 0000", "1111 1111 1111 1111", "1",
			"1111 1111 1111 1111"),
		NewData("1111 1111 1111 1111", "0000 0000 0000 0000", "0",
			"1111 1111 1111 1111"),
		NewData("1111 1111 1111 1111", "0000 0000 0000 0000", "1",
			"0000 0000 0000 0000"),
	}
	for _, data := range testData {
		input := &Mux16InputPins{
			A:   data.NextWord(),
			B:   data.NextWord(),
			Sel: data.NextBool(),
		}

		expected := Out16Pins{
			Out: data.NextWord(),
		}

		result := *MUX16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_MUX4Way(t *testing.T) {
	testData := []*Data{
		NewData("1", "0", "0", "0", "00",
			"1"),
		NewData("0", "1", "0", "0", "01",
			"1"),
		NewData("0", "0", "1", "0", "10",
			"1"),
		NewData("0", "0", "0", "1", "11",
			"1"),
	}
	for _, data := range testData {
		input := &Mux4WayInputPins{
			A:   data.NextBool(),
			B:   data.NextBool(),
			C:   data.NextBool(),
			D:   data.NextBool(),
			Sel: data.Next2Bool(),
		}

		expected := OutPins{
			Out: data.NextBool(),
		}

		result := *MUX4Way(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_MUX4Way16(t *testing.T) {
	testData := []*Data{
		NewData("1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "00",
			"1111 1111 1111 1111"),
		NewData("0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "01",
			"1111 1111 1111 1111"),
		NewData("0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "10",
			"1111 1111 1111 1111"),
		NewData("0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "11",
			"1111 1111 1111 1111"),
	}
	for _, data := range testData {
		input := &Mux4Way16InputPins{
			A:   data.NextWord(),
			B:   data.NextWord(),
			C:   data.NextWord(),
			D:   data.NextWord(),
			Sel: data.Next2Bool(),
		}

		expected := Out16Pins{
			Out: data.NextWord(),
		}

		result := *MUX4Way16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_MUX8Way(t *testing.T) {
	testData := []*Data{
		NewData("1", "0", "0", "0", "0", "0", "0", "0", "000",
			"1"),
		NewData("0", "1", "0", "0", "0", "0", "0", "0", "001",
			"1"),
		NewData("0", "0", "1", "0", "0", "0", "0", "0", "010",
			"1"),
		NewData("0", "0", "0", "1", "0", "0", "0", "0", "011",
			"1"),

		NewData("0", "0", "0", "0", "1", "0", "0", "0", "100",
			"1"),
		NewData("0", "0", "0", "0", "0", "1", "0", "0", "101",
			"1"),
		NewData("0", "0", "0", "0", "0", "0", "1", "0", "110",
			"1"),
		NewData("0", "0", "0", "0", "0", "0", "0", "1", "111",
			"1"),
	}
	for _, data := range testData {
		input := &Mux8WayInputPins{
			A:   data.NextBool(),
			B:   data.NextBool(),
			C:   data.NextBool(),
			D:   data.NextBool(),
			E:   data.NextBool(),
			F:   data.NextBool(),
			G:   data.NextBool(),
			H:   data.NextBool(),
			Sel: data.Next3Bool(),
		}

		expected := OutPins{
			Out: data.NextBool(),
		}

		result := *MUX8Way(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_MUX8Way16(t *testing.T) {
	testData := []*Data{
		NewData("1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "000",
			"1111 1111 1111 1111"),
		NewData("0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "001",
			"1111 1111 1111 1111"),
		NewData("0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "010",
			"1111 1111 1111 1111"),
		NewData("0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "011",
			"1111 1111 1111 1111"),

		NewData("0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "100",
			"1111 1111 1111 1111"),
		NewData("0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "101",
			"1111 1111 1111 1111"),
		NewData("0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "110",
			"1111 1111 1111 1111"),
		NewData("0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "111",
			"1111 1111 1111 1111"),
	}
	for _, data := range testData {
		input := &Mux8Way16InputPins{
			A:   data.NextWord(),
			B:   data.NextWord(),
			C:   data.NextWord(),
			D:   data.NextWord(),
			E:   data.NextWord(),
			F:   data.NextWord(),
			G:   data.NextWord(),
			H:   data.NextWord(),
			Sel: data.Next3Bool(),
		}

		expected := Out16Pins{
			Out: data.NextWord(),
		}

		result := *MUX8Way16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_DMUX(t *testing.T) {
	testData := []*Data{
		NewData("0", "0",
			"0", "0"),
		NewData("1", "0",
			"1", "0"),
		NewData("0", "1",
			"0", "0"),
		NewData("1", "1",
			"0", "1"),
	}
	for _, data := range testData {
		input := &DMuxInputPins{
			In:  data.NextBool(),
			Sel: data.NextBool(),
		}

		expected := ABPins{
			A: data.NextBool(),
			B: data.NextBool(),
		}

		result := *DMUX(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_DMUX16(t *testing.T) {
	testData := []*Data{
		NewData("0000 0000 0000 0000", "0",
			"0000 0000 0000 0000", "0000 0000 0000 0000"),
		NewData("1111 1111 1111 1111", "0",
			"1111 1111 1111 1111", "0000 0000 0000 0000"),
		NewData("0000 0000 0000 0000", "1",
			"0000 0000 0000 0000", "0000 0000 0000 0000"),
		NewData("1111 1111 1111 1111", "1",
			"0000 0000 0000 0000", "1111 1111 1111 1111"),
	}
	for _, data := range testData {
		input := &DMux16InputPins{
			In:  data.NextWord(),
			Sel: data.NextBool(),
		}

		expected := AB16Pins{
			A: data.NextWord(),
			B: data.NextWord(),
		}

		result := *DMUX16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_DMUX4Way(t *testing.T) {
	testData := []*Data{
		NewData("1", "00",
			"1", "0", "0", "0"),
		NewData("1", "01",
			"0", "1", "0", "0"),
		NewData("1", "10",
			"0", "0", "1", "0"),
		NewData("1", "11",
			"0", "0", "0", "1"),
	}
	for _, data := range testData {
		input := &DMux4WayInputPins{
			In:  data.NextBool(),
			Sel: data.Next2Bool(),
		}

		expected := DMux4WayOutPins{
			A: data.NextBool(),
			B: data.NextBool(),
			C: data.NextBool(),
			D: data.NextBool(),
		}

		result := *DMUX4Way(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_DMUX4Way16(t *testing.T) {
	testData := []*Data{
		NewData("1111 1111 1111 1111", "00",
			"1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000"),
		NewData("1111 1111 1111 1111", "01",
			"0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000"),
		NewData("1111 1111 1111 1111", "10",
			"0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000"),
		NewData("1111 1111 1111 1111", "11",
			"0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111"),
	}
	for _, data := range testData {
		input := &DMux4Way16InputPins{
			In:  data.NextWord(),
			Sel: data.Next2Bool(),
		}

		expected := DMux4Way16OutPins{
			A: data.NextWord(),
			B: data.NextWord(),
			C: data.NextWord(),
			D: data.NextWord(),
		}

		result := *DMUX4Way16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_DMUX8Way(t *testing.T) {
	testData := []*Data{
		NewData("1", "000",
			"1", "0", "0", "0", "0", "0", "0", "0"),
		NewData("1", "001",
			"0", "1", "0", "0", "0", "0", "0", "0"),
		NewData("1", "010",
			"0", "0", "1", "0", "0", "0", "0", "0"),
		NewData("1", "011",
			"0", "0", "0", "1", "0", "0", "0", "0"),

		NewData("1", "100",
			"0", "0", "0", "0", "1", "0", "0", "0"),
		NewData("1", "101",
			"0", "0", "0", "0", "0", "1", "0", "0"),
		NewData("1", "110",
			"0", "0", "0", "0", "0", "0", "1", "0"),
		NewData("1", "111",
			"0", "0", "0", "0", "0", "0", "0", "1"),
	}
	for _, data := range testData {
		input := &DMux8WayInputPins{
			In:  data.NextBool(),
			Sel: data.Next3Bool(),
		}

		expected := DMux8WayOutPins{
			A: data.NextBool(),
			B: data.NextBool(),
			C: data.NextBool(),
			D: data.NextBool(),

			E: data.NextBool(),
			F: data.NextBool(),
			G: data.NextBool(),
			H: data.NextBool(),
		}

		result := *DMUX8Way(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_DMUX8Way16(t *testing.T) {
	testData := []*Data{
		NewData("1111 1111 1111 1111", "000",
			"1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000"),
		NewData("1111 1111 1111 1111", "001",
			"0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000"),
		NewData("1111 1111 1111 1111", "010",
			"0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000"),
		NewData("1111 1111 1111 1111", "011",
			"0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000"),

		NewData("1111 1111 1111 1111", "100",
			"0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000"),
		NewData("1111 1111 1111 1111", "101",
			"0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000", "0000 0000 0000 0000"),
		NewData("1111 1111 1111 1111", "110",
			"0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111", "0000 0000 0000 0000"),
		NewData("1111 1111 1111 1111", "111",
			"0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "0000 0000 0000 0000", "1111 1111 1111 1111"),
	}
	for _, data := range testData {
		input := &DMux8Way16InputPins{
			In:  data.NextWord(),
			Sel: data.Next3Bool(),
		}

		expected := DMux8Way16OutPins{
			A: data.NextWord(),
			B: data.NextWord(),
			C: data.NextWord(),
			D: data.NextWord(),

			E: data.NextWord(),
			F: data.NextWord(),
			G: data.NextWord(),
			H: data.NextWord(),
		}

		result := *DMUX8Way16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_DMUX256Way(t *testing.T) {
	bits := [3]int{0, int(rand.Int31n(byteSize - 1)), byteSize - 1}
	var testData []*Data
	for _, bit := range bits {
		bitStr := utils.NumToBinString(bit, 8)
		testData = append(testData,
			NewData("1", bitStr).WithKeyValue("bit", bit),
		)
	}
	for _, data := range testData {
		input := &DMux256WayInputPins{
			In:  data.NextBool(),
			Sel: data.NextByte(),
		}

		var out [256]bool
		out[data.GetIntValue("bit")] = true
		expected := DMux256WayOutPins{
			Out: out,
		}

		result := *DMUX256Way(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}
