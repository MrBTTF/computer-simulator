package component

import (
	"computer-simulator/pkg/utils"
	"fmt"
	"strconv"
)

type Address [adressSize]bool

func AddressFromInt(i int) Address {
	var bin Address
	copy(bin[:], utils.NumToBin(i, adressSize))
	return bin
}
func AddressFromWord(w Word) Address {
	var a Address
	copy(a[:], w[:])
	return a
}

func (adr *Address) String() string {
	return utils.BinToString(adr[:])
}

func (adr *Address) Int() int {
	address, err := strconv.ParseInt(utils.BinToString(adr[:]), 2, adressSize+1)
	if err != nil {
		panic(err)
	}
	if address >= ram16KSize {
		panic(fmt.Sprintf("addres %d is out of bound, memory size: %d", address, ram16KSize))
	}
	return int(address)
}

type Memory map[int]Byte

type DFFInputPins struct {
	In      bool
	ClockIn bool
	Out     bool
}
type BitInputPins struct {
	In      bool
	ClockIn bool
	Load    bool
	Enable  bool
	Out     bool
}

type Reg8InputPins struct {
	In      Byte
	ClockIn bool
	Load    bool
	Enable  bool
	Out     Byte
}

type Reg16InputPins struct {
	In      Word
	ClockIn bool
	Load    bool
	Enable  bool
	Out     Word
}

type Reg2BytesInputPins struct {
	In      Byte
	ClockIn bool
	Load0   bool
	Load1   bool
	Enable  bool
	Out     Word
}

type RAM16KInputPins struct {
	In      Byte
	Load    bool
	ClockIn bool
	Address Address
	Enable  bool
	Out     Memory
}

type PCInputPins struct {
	In      Word
	ClockIn bool
	Load    bool
	Inc     bool
	Reset   bool
	Out     Word
}

type MemOutPins struct {
	NextOut bool
	Out     bool
}

type MemOut8Pins struct {
	NextOut Byte
	Out     Byte
}

type MemOut16Pins struct {
	NextOut Word
	Out     Word
}

type RAM16KOutPins struct {
	Out       Byte
	MemoryOut Memory
}

func DFF(input *DFFInputPins) *MemOutPins {
	notIn := NOT(&InPins{input.In})

	nand1 := NAND(&ABPins{input.In, input.ClockIn})
	nand2 := NAND(&ABPins{notIn.Out, input.ClockIn})
	nand4 := NAND(&ABPins{nand2.Out, input.Out})
	nand3 := NAND(&ABPins{nand4.Out, nand1.Out})

	return &MemOutPins{
		NextOut: nand3.Out,
		Out:     input.Out,
	}
}

func BIT(input *BitInputPins) *MemOutPins {
	mux := MUX(&MuxInputPins{input.Out, input.In, input.Load})
	dff := DFF(&DFFInputPins{mux.Out, input.ClockIn, input.Out})
	and := AND(&ABPins{dff.Out, input.Enable})

	return &MemOutPins{
		NextOut: dff.NextOut,
		Out:     and.Out,
	}
}

func REG8(input *Reg8InputPins) *MemOut8Pins {
	var out MemOut8Pins
	for i := 0; i < len(input.In); i++ {
		bit := BIT(&BitInputPins{
			input.In[i],
			input.ClockIn,
			input.Load,
			input.Enable,
			input.Out[i],
		})
		out.NextOut[i] = bit.NextOut
		out.Out[i] = bit.Out
	}

	return &out
}

func REG16(input *Reg16InputPins) *MemOut16Pins {
	var byteIn, byteOut Byte

	copy(byteIn[:], input.In[:byteSize])
	copy(byteOut[:], input.Out[:byteSize])

	input16 := &Reg8InputPins{
		In:      byteIn,
		ClockIn: input.ClockIn,
		Load:    input.Load,
		Enable:  input.Enable,
		Out:     byteOut,
	}
	reg1 := REG8(input16)

	copy(byteIn[:], input.In[byteSize:])
	copy(byteOut[:], input.Out[byteSize:])
	input16.In = byteIn
	input16.Out = byteOut
	reg2 := REG8(input16)

	var out MemOut16Pins
	copy(out.NextOut[:byteSize], reg1.NextOut[:])
	copy(out.NextOut[byteSize:], reg2.NextOut[:])

	copy(out.Out[:byteSize], reg1.Out[:])
	copy(out.Out[byteSize:], reg2.Out[:])

	return &out
}

func REG2Bytes(input *Reg2BytesInputPins) *MemOut16Pins {
	var out MemOut16Pins

	reg1 := REG8(&Reg8InputPins{
		In:      input.In,
		ClockIn: input.ClockIn,
		Load:    input.Load0,
		Enable:  input.Enable,
		Out:     input.Out.FirstByte(),
	})

	reg2 := REG8(&Reg8InputPins{
		In:      input.In,
		ClockIn: input.ClockIn,
		Load:    input.Load1,
		Enable:  input.Enable,
		Out:     input.Out.SecondByte(),
	})

	copy(out.NextOut[:], append(reg1.NextOut[:], reg2.NextOut[:]...))
	copy(out.Out[:], append(reg1.Out[:], reg2.Out[:]...))

	return &out
}

func RAM16K(input *RAM16KInputPins) *RAM16KOutPins {
	address, err := strconv.ParseInt(utils.BinToString(input.Address[:]), 2, 15)
	if err != nil {
		panic(err)
	}
	if address >= ram16KSize {
		panic(fmt.Sprintf("address %d is out of bound, memory size: %d", address, ram16KSize))
	}

	reg8 := REG8(&Reg8InputPins{
		input.In,
		input.ClockIn,
		input.Load,
		input.Enable,
		input.Out[int(address)],
	})

	input.Out[int(address)] = reg8.NextOut

	out := &RAM16KOutPins{
		Out:       reg8.Out,
		MemoryOut: input.Out,
	}

	return out
}

func PC(input *PCInputPins) *MemOut16Pins {
	inc := Inc16(&In16Pins{
		In: input.Out,
	})

	mux1 := MUX16(&Mux16InputPins{
		A:   input.Out,
		B:   inc.Out,
		Sel: input.Inc,
	})
	mux2 := MUX16(&Mux16InputPins{
		A:   mux1.Out,
		B:   input.In,
		Sel: input.Load,
	})
	mux3 := MUX16(&Mux16InputPins{
		A:   mux2.Out,
		Sel: input.Reset,
	})

	reg16 := REG16(&Reg16InputPins{
		In:      mux3.Out,
		ClockIn: input.ClockIn,
		Load:    true,
		Enable:  true,
		Out:     input.Out,
	})
	return &MemOut16Pins{
		NextOut: reg16.NextOut,
		Out:     reg16.Out,
	}
}
