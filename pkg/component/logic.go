package component

type ComparatorOutPins struct {
	Out     Word
	ALarger bool
	Eq      bool
}

func NAND(input *ABPins) *OutPins {
	output := !(input.A && input.B)
	return &OutPins{Out: output}
}

func NOT(input *InPins) *OutPins {
	nandOutput := NAND(&ABPins{input.In, input.In})
	return nandOutput
}

func AND(input *ABPins) *OutPins {
	nandOutput := NAND(input)
	notOutput := NOT(&InPins{nandOutput.Out})
	return notOutput
}

func AND3Way(input *ABCPins) *OutPins {
	and := AND(&ABPins{
		A: input.A,
		B: input.B,
	})
	return AND(&ABPins{
		A: and.Out,
		B: input.C,
	})
}

func OR(input *ABPins) *OutPins {
	notA := NOT(&InPins{input.A})
	notB := NOT(&InPins{input.B})

	nand := NAND(&ABPins{notA.Out, notB.Out})

	return nand
}

func XOR(input *ABPins) *OutPins {
	notA := NOT(&InPins{input.A})
	notB := NOT(&InPins{input.B})

	and1 := AND(&ABPins{input.A, notB.Out})
	and2 := AND(&ABPins{notA.Out, input.B})

	or := OR(&ABPins{and1.Out, and2.Out})

	return or
}

func NOT16(input *In16Pins) *Out16Pins {
	var out Out16Pins

	for i := 0; i < wordSize; i++ {
		out.Out[i] = NOT(&InPins{input.In[i]}).Out
	}

	return &out
}

func AND16(input *AB16Pins) *Out16Pins {
	return make16AB(AND, input)
}

func OR16(input *AB16Pins) *Out16Pins {
	return make16AB(OR, input)
}

func OR16Way(input *In16Pins) *OutPins {
	var out OutPins

	out.Out = OR(&ABPins{
		A: input.In[0],
		B: input.In[1],
	}).Out
	for i := 2; i < wordSize; i++ {
		out.Out = OR(&ABPins{
			A: out.Out,
			B: input.In[i],
		}).Out
	}

	return &out
}

func Comparator16(input *AB16Pins) *ComparatorOutPins {
	var out ComparatorOutPins

	out.Eq = true

	for i := wordSize - 1; i >= 0; i-- {
		uneq := XOR(&ABPins{
			A: input.A[i],
			B: input.B[i],
		})
		out.Out[i] = uneq.Out

		and1 := AND(&ABPins{
			A: NOT(&InPins{uneq.Out}).Out,
			B: out.Eq,
		})

		and2 := AND(&ABPins{
			A: out.Eq,
			B: input.A[i],
		})

		and3 := AND(&ABPins{
			A: and2.Out,
			B: uneq.Out,
		})

		or := OR(&ABPins{
			A: and3.Out,
			B: out.ALarger,
		})

		out.Eq = and1.Out
		out.ALarger = or.Out
	}

	return &out
}
