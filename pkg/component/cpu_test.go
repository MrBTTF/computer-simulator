package component

import (
	"computer-simulator/pkg/utils"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

var (
	emptyWord = "0000 0000 0000 0000"
	emptyByte = "0000 0000"

	R0 = "00"
	R1 = "01"
	R2 = "10"
	R3 = "11"
)

func MOV_RX_RY(rx, ry string) string {
	return "0000 0000 0000 " + rx + ry
}

func MOV_RX_refRY(rx, ry string) string {
	return "0000 0000 0001 " + rx + ry
}

func MOV_refRX_RY(rx, ry string) string {
	return "0000 0000 0010 " + rx + ry
}

func MOV_refRX_refRY(rx, ry string) string {
	return "0000 0000 0011 " + rx + ry
}

func MOV_RX_value(rx string) string {
	return "0000 0000 0100 " + rx + "00"
}

func ADD_RX_RY(rx, ry string) string {
	return "0000 0000 1000 " + rx + ry
}

func NOT_RX(rx string) string {
	return "0000 0000 1011 00" + rx
}

func Test_BUS(t *testing.T) {
	testData := []*Data{
		NewData("1000 0000 0000 0001", "0100 0000 0100 0000",
			"1100 0000 0100 0001"),
	}
	for _, data := range testData {
		input := &BusInputPins{
			In:  data.NextWord(),
			Out: data.NextWord(),
		}

		expected := Out16Pins{
			Out: data.NextWord(),
		}

		result := *BUS(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_FetchExecute(t *testing.T) {
	instr1Str := "0100 1101 1011 0111"
	instr1FirstByteStr := "0000 0000 1011 0111"
	instr1SecondByteStr := "0000 0000 0100 1101"

	instr2Str := "0111 0001 0011 0101"
	instr2FirstByteStr := "0000 0000 0011 0101"
	instr2SecondByteStr := "0000 0000 0111 0001"

	var instr1, instr2 Word
	copy(instr1[:], utils.StringToBin(instr1Str, wordSize))
	copy(instr2[:], utils.StringToBin(instr2Str, wordSize))

	ramOut := make(Memory)
	ramOut[0] = instr1.FirstByte()
	ramOut[1] = instr1.SecondByte()
	ramOut[2] = instr2.FirstByte()
	ramOut[3] = instr2.SecondByte()

	var fsmOut [6]string
	var pcOut [5]string
	for i := 0; i < 6; i++ {
		fsmOut[i] = utils.NumToBinString(i, wordSize)
	}
	for i := 0; i < 5; i++ {
		pcOut[i] = utils.NumToBinString(i, wordSize)
	}

	testData := []*Data{
		NewData("1", emptyWord, fsmOut[0], pcOut[0], emptyWord,
			instr1FirstByteStr, fsmOut[1], pcOut[1], instr1FirstByteStr),
		NewData("0", emptyWord, fsmOut[1], pcOut[1], instr1FirstByteStr,
			instr1SecondByteStr, fsmOut[1], pcOut[1], instr1FirstByteStr),

		NewData("1", instr1SecondByteStr, fsmOut[1], pcOut[1], instr1FirstByteStr,
			instr1SecondByteStr, fsmOut[2], pcOut[2], instr1Str),
		NewData("0", emptyWord, fsmOut[2], pcOut[2], instr1Str,
			emptyWord, fsmOut[2], pcOut[2], instr1Str),

		NewData("1", instr1SecondByteStr, fsmOut[2], pcOut[2], instr1Str,
			instr1SecondByteStr, fsmOut[3], pcOut[2], instr1Str),
		NewData("0", emptyWord, fsmOut[3], pcOut[2], instr1Str,
			emptyWord, fsmOut[3], pcOut[2], instr1Str),

		NewData("1", instr1SecondByteStr, fsmOut[3], pcOut[2], instr1Str,
			instr1SecondByteStr, fsmOut[4], pcOut[2], instr1Str),
		NewData("0", emptyWord, fsmOut[4], pcOut[2], instr1Str,
			emptyWord, fsmOut[4], pcOut[2], instr1Str),

		NewData("1", emptyWord, fsmOut[4], pcOut[2], instr1Str,
			emptyWord, fsmOut[5], pcOut[2], instr1Str),
		NewData("0", emptyWord, fsmOut[5], pcOut[2], instr1Str,
			emptyWord, fsmOut[5], pcOut[2], instr1Str),

		NewData("1", emptyWord, fsmOut[5], pcOut[2], instr1Str,
			emptyWord, fsmOut[0], pcOut[2], instr1Str),
		NewData("0", emptyWord, fsmOut[0], pcOut[2], instr1Str,
			instr2FirstByteStr, fsmOut[0], pcOut[2], instr1Str),

		NewData("1", emptyWord, fsmOut[0], pcOut[2], emptyWord,
			instr2FirstByteStr, fsmOut[1], pcOut[3], instr2FirstByteStr),
		NewData("0", emptyWord, fsmOut[1], pcOut[3], instr2FirstByteStr,
			instr2SecondByteStr, fsmOut[1], pcOut[3], instr2FirstByteStr),

		NewData("1", instr2SecondByteStr, fsmOut[1], pcOut[3], instr2FirstByteStr,
			instr2SecondByteStr, fsmOut[2], pcOut[4], instr2Str),
		NewData("0", emptyWord, fsmOut[2], pcOut[4], instr2Str,
			emptyWord, fsmOut[2], pcOut[4], instr2Str),

		NewData("1", emptyWord, fsmOut[2], pcOut[4], instr2Str,
			emptyWord, fsmOut[3], pcOut[4], instr2Str),
		NewData("0", emptyWord, fsmOut[3], pcOut[4], instr2Str,
			emptyWord, fsmOut[3], pcOut[4], instr2Str),

		NewData("1", emptyWord, fsmOut[3], pcOut[4], instr2Str,
			emptyWord, fsmOut[4], pcOut[4], instr2Str),
		NewData("0", emptyWord, fsmOut[4], pcOut[4], instr2Str,
			emptyWord, fsmOut[4], pcOut[4], instr2Str),

		NewData("1", emptyWord, fsmOut[4], pcOut[4], instr2Str,
			emptyWord, fsmOut[5], pcOut[4], instr2Str),
		NewData("0", emptyWord, fsmOut[5], pcOut[4], instr2Str,
			emptyWord, fsmOut[5], pcOut[4], instr2Str),

		NewData("1", emptyWord, fsmOut[5], pcOut[4], instr2Str,
			emptyWord, fsmOut[0], pcOut[4], instr2Str),
		NewData("0", emptyWord, fsmOut[0], pcOut[4], instr2Str,
			emptyWord, fsmOut[0], pcOut[4], instr2Str),
	}

	for _, data := range testData {
		input := &FetchExecuteInPins{
			ClockIn: data.NextBool(),
			BusOut:  data.NextWord(),
			FSMOut:  data.NextWord(),
			PCOut:   data.NextWord(),
			IROut:   data.NextWord(),
			RAMOut:  ramOut,
		}

		expected := FetchExecuteOutPins{
			BusOut: data.NextWord(),
			FSMOut: data.NextWord(),
			PCOut:  data.NextWord(),
			IROut:  data.NextWord(),
		}

		result := *FetchExecute(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}

}

func Test_ReadRegister(t *testing.T) {
	value0 := "0100 1101 1011 0111"
	value1 := "0101 1101 1011 0111"
	value2 := "0100 1101 1111 0111"
	value3 := "0100 1111 1011 0111"

	testData := []*Data{
		NewData("1", "1", "00", value0, value1, value2, value3, emptyWord,
			value0),
		NewData("1", "1", "01", value0, value1, value2, value3, emptyWord,
			value1),
		NewData("1", "1", "10", value0, value1, value2, value3, emptyWord,
			value2),
		NewData("1", "1", "11", value0, value1, value2, value3, emptyWord,
			value3),

		NewData("1", "0", "00", value0, value1, value2, value3, emptyWord,
			emptyWord),
	}

	for _, data := range testData {
		input := &ReadRegisterInPins{
			ClockIn: data.NextBool(),
			Control: data.NextBool(),
			Address: data.Next2Bool(),
			R0Out:   data.NextWord(),
			R1Out:   data.NextWord(),
			R2Out:   data.NextWord(),
			R3Out:   data.NextWord(),
			BusOut:  data.NextWord(),
		}

		expected := data.NextWord()

		result := *ReadRegister(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_WriteRegister(t *testing.T) {
	value0 := "0100 1101 1011 0111"
	value1 := "0101 1101 1011 0111"
	value2 := "0100 1101 1111 0111"
	value3 := "0100 1111 1011 0111"

	testData := []*Data{
		NewData("1", "1", "00", value0,
			value0, emptyWord, emptyWord, emptyWord),
		NewData("1", "1", "01", value1,
			emptyWord, value1, emptyWord, emptyWord),
		NewData("1", "1", "10", value2,
			emptyWord, emptyWord, value2, emptyWord),
		NewData("1", "1", "11", value3,
			emptyWord, emptyWord, emptyWord, value3),

		NewData("1", "0", "00", value0,
			emptyWord, emptyWord, emptyWord, emptyWord),
	}

	for _, data := range testData {
		input := &ReadRegisterInPins{
			ClockIn: data.NextBool(),
			Control: data.NextBool(),
			Address: data.Next2Bool(),
			BusOut:  data.NextWord(),
		}

		expected := WriteRegisterOutPins{
			R0Out: data.NextWord(),
			R1Out: data.NextWord(),
			R2Out: data.NextWord(),
			R3Out: data.NextWord(),
		}

		result := *WriteRegister(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_MovRxRy(t *testing.T) {
	instr := MOV_RX_RY("00", "01")
	value0 := "0100 1101 1011 0111"
	value1 := "0101 1101 1011 0111"

	testData := []*Data{
		NewData("1", "1", "1", "0", instr, value0, value1, emptyWord, emptyWord, emptyWord,
			value0, value1, emptyWord, emptyWord, value1),
		NewData("1", "1", "0", "1", instr, value0, value1, emptyWord, emptyWord, emptyWord,
			value1, value1, emptyWord, emptyWord, value1),
	}

	for _, data := range testData {
		input := &MovRxRyInPins{
			ClockIn: data.NextBool(),
			Control: data.NextBool(),
			State2:  data.NextBool(),
			State3:  data.NextBool(),
			IROut:   data.NextWord(),
			R0Out:   data.NextWord(),
			R1Out:   data.NextWord(),
			R2Out:   data.NextWord(),
			R3Out:   data.NextWord(),
			BusOut:  data.NextWord(),
		}

		expected := MovRxRyOutPins{
			R0Out:  data.NextWord(),
			R1Out:  data.NextWord(),
			R2Out:  data.NextWord(),
			R3Out:  data.NextWord(),
			BusOut: data.NextWord(),
		}

		result := *MovRxRy(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_MovRxRefRy(t *testing.T) {
	instr := MOV_RX_refRY("00", "01")
	value0Str := "0100 1101 1011 0111"
	addressStr := "0000 0000 0000 0010"
	value1Str := "0000 0000 1011 0111"

	var value1 Word
	copy(value1[:], utils.StringToBin(value1Str, wordSize))

	ramOut := make(Memory)
	ramOut[2] = value1.FirstByte()

	testData := []*Data{
		NewData("1", "1", "1", "0", instr, value0Str, addressStr, emptyWord, emptyWord, emptyWord, emptyWord,
			value0Str, addressStr, emptyWord, emptyWord, emptyWord, addressStr),
		NewData("1", "1", "0", "1", instr, value0Str, addressStr, emptyWord, emptyWord, value1Str, addressStr,
			value1Str, addressStr, emptyWord, emptyWord, value1Str, addressStr),
	}

	for _, data := range testData {
		input := &MovRxRyInPins{
			ClockIn:    data.NextBool(),
			Control:    data.NextBool(),
			State2:     data.NextBool(),
			State3:     data.NextBool(),
			IROut:      data.NextWord(),
			R0Out:      data.NextWord(),
			R1Out:      data.NextWord(),
			R2Out:      data.NextWord(),
			R3Out:      data.NextWord(),
			BusOut:     data.NextWord(),
			AddressOut: data.NextWord(),
			RAMOut:     ramOut,
		}

		expected := MovRxRyOutPins{
			R0Out:      data.NextWord(),
			R1Out:      data.NextWord(),
			R2Out:      data.NextWord(),
			R3Out:      data.NextWord(),
			BusOut:     data.NextWord(),
			AddressOut: data.NextWord(),
			RAMOut:     ramOut,
		}

		result := *MovRxRefRy(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_MovRefRxRy(t *testing.T) {
	instr := MOV_refRX_RY("00", "01")
	addressStr := "0000 0000 0000 0010"
	value1Str := "0000 0000 1011 0111"

	var value1 Word
	copy(value1[:], utils.StringToBin(value1Str, wordSize))

	ramIn := make(Memory)
	var empty Byte
	copy(empty[:], utils.StringToBin(emptyByte, byteSize)[:])
	ramIn[2] = empty

	ramOut := make(Memory)
	ramOut[2] = value1.FirstByte()

	testData := []*Data{
		NewData("1", "1", "1", "0", instr, addressStr, value1Str, emptyWord, emptyWord, emptyWord, emptyWord,
			addressStr, value1Str, emptyWord, emptyWord, addressStr, addressStr).WithKeyValue("memory", ramIn),
		NewData("1", "1", "0", "1", instr, addressStr, value1Str, emptyWord, emptyWord, value1Str, addressStr,
			addressStr, value1Str, emptyWord, emptyWord, value1Str, addressStr).WithKeyValue("memory", ramOut),
	}

	for _, data := range testData {
		input := &MovRxRyInPins{
			ClockIn:    data.NextBool(),
			Control:    data.NextBool(),
			State2:     data.NextBool(),
			State3:     data.NextBool(),
			IROut:      data.NextWord(),
			R0Out:      data.NextWord(),
			R1Out:      data.NextWord(),
			R2Out:      data.NextWord(),
			R3Out:      data.NextWord(),
			BusOut:     data.NextWord(),
			AddressOut: data.NextWord(),
			RAMOut:     data.GetMemoryValue("memory"),
		}

		expected := MovRxRyOutPins{
			R0Out:      data.NextWord(),
			R1Out:      data.NextWord(),
			R2Out:      data.NextWord(),
			R3Out:      data.NextWord(),
			BusOut:     data.NextWord(),
			AddressOut: data.NextWord(),
			RAMOut:     data.GetMemoryValue("memory"),
		}

		result := *MovRefRxRy(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_SendToALU(t *testing.T) {
	instr := ADD_RX_RY("00", "01")
	value0 := "0001 0000 1000 0001"
	value1 := "0000 0000 1000 0010"
	sum := "0001 0001 0000 0011"

	testData := []*Data{
		NewData("1", "1", "1", "0", "0", "0", value0, value1, emptyWord, emptyWord, instr, emptyWord, emptyWord, emptyWord,
			"0", "0", "0", "0", value0, value1, emptyWord, emptyWord, value1, value1, emptyWord),
		NewData("1", "1", "0", "1", "0", "0", value0, value1, emptyWord, emptyWord, instr, value0, value1, emptyWord,
			"0", "0", "0", "0", value0, value1, emptyWord, emptyWord, value0, value1, sum),
		NewData("1", "1", "0", "0", "1", "0", value0, value1, emptyWord, emptyWord, instr, emptyWord, value1, sum,
			"0", "0", "1", "1", sum, value1, emptyWord, emptyWord, sum, value1, sum),
	}

	for _, data := range testData {
		input := &SendToALUInPins{
			ClockIn: data.NextBool(),
			Control: data.NextBool(),
			State2:  data.NextBool(),
			State3:  data.NextBool(),
			State4:  data.NextBool(),
			CarryIn: data.NextBool(),
			R0Out:   data.NextWord(),
			R1Out:   data.NextWord(),
			R2Out:   data.NextWord(),
			R3Out:   data.NextWord(),
			IROut:   data.NextWord(),
			BusOut:  data.NextWord(),
			TMPOut:  data.NextWord(),
			ACCOut:  data.NextWord(),
		}

		expected := SendToALUOutPins{
			Flags: FlagsPins{
				CarryOut: data.NextBool(),
				ALarger:  data.NextBool(),
				Eq:       data.NextBool(),
				Zero:     data.NextBool(),
			},
			R0Out:  data.NextWord(),
			R1Out:  data.NextWord(),
			R2Out:  data.NextWord(),
			R3Out:  data.NextWord(),
			BusOut: data.NextWord(),
			TMPOut: data.NextWord(),
			ACCOut: data.NextWord(),
		}

		result := *SendToALU(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_CPU_MovRxRy(t *testing.T) {
	cpu := new(CPU)
	value0 := "0001 0000 1000 0001"
	value1 := "0000 0000 1000 0010"
	value2 := "0000 0000 1010 0010"
	movR0R1 := MOV_RX_RY("00", "01")
	movR0R1Half := "0000 0000 " + movR0R1[len(movR0R1)/2:]
	movR0R2 := MOV_RX_RY("00", "10")
	movR0R2Half := "0000 0000 " + movR0R2[len(movR0R2)/2:]

	data := NewData(value0, value1, value2, movR0R1, movR0R2)

	cpu.r0Out = data.NextWord()
	cpu.r1Out = data.NextWord()
	cpu.r2Out = data.NextWord()

	movR0R1Val := data.NextWord()
	movR0R2Val := data.NextWord()
	ramOut := Memory{
		0: movR0R1Val.FirstByte(),
		1: movR0R1Val.SecondByte(),
		2: movR0R2Val.FirstByte(),
		3: movR0R2Val.SecondByte(),
		4: Byte{},
	}
	cpu.ramOut = ramOut

	var fsmOut [6]string
	var pcOut [5]string
	for i := 0; i < 6; i++ {
		fsmOut[i] = utils.NumToBinString(i, wordSize)
	}
	for i := 0; i < 5; i++ {
		pcOut[i] = utils.NumToBinString(i, wordSize)
	}

	testData := []*Data{
		NewData(movR0R1Half, movR0R1Half, fsmOut[1], pcOut[1],
			value0, value1, value2, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0R1, emptyWord, fsmOut[2], pcOut[2],
			value0, value1, value2, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0R1, value1, fsmOut[3], pcOut[2],
			value0, value1, value2, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0R1, value1, fsmOut[4], pcOut[2],
			value1, value1, value2, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0R1, emptyWord, fsmOut[5], pcOut[2],
			value1, value1, value2, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0R1, emptyWord, fsmOut[0], pcOut[2],
			value1, value1, value2, emptyWord).WithKeyValue("memory", ramOut),

		NewData(movR0R2Half, movR0R2Half, fsmOut[1], pcOut[3],
			value1, value1, value2, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0R2, emptyWord, fsmOut[2], pcOut[4],
			value1, value1, value2, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0R2, value2, fsmOut[3], pcOut[4],
			value1, value1, value2, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0R2, value2, fsmOut[4], pcOut[4],
			value2, value1, value2, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0R2, emptyWord, fsmOut[5], pcOut[4],
			value2, value1, value2, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0R2, emptyWord, fsmOut[0], pcOut[4],
			value2, value1, value2, emptyWord).WithKeyValue("memory", ramOut),
	}

	for _, data := range testData {
		cpu.step(true)
		// fmt.Println("FSM: ", cpu.fsmOut, ", PC: ", cpu.pcOut)
		assert.Equal(t, data.NextWord(), cpu.irOut)
		assert.Equal(t, data.NextWord(), cpu.busOut)
		assert.Equal(t, data.NextWord(), cpu.fsmOut)
		assert.Equal(t, data.NextWord(), cpu.pcOut)

		assert.Equal(t, data.NextWord(), cpu.r0Out)
		assert.Equal(t, data.NextWord(), cpu.r1Out)
		assert.Equal(t, data.NextWord(), cpu.r2Out)
		assert.Equal(t, data.NextWord(), cpu.r3Out)

		assert.Equal(t, data.GetMemoryValue("memory"), cpu.ramOut)
	}
}

func Test_CPU_MovRxRefRy(t *testing.T) {
	cpu := new(CPU)
	value1 := "0000 0000 1000 0001"
	value2 := "0000 0000 1011 0111"
	address1 := "0000 0000 0000 0100"
	address2 := "0000 0000 0000 0101"
	value1val := WordFromString(value1)
	value2val := WordFromString(value2)

	movR0RefR1 := MOV_RX_refRY("00", "01")
	movR0RefR1Half := "0000 0000 " + movR0RefR1[len(movR0RefR1)/2:]
	movR0RefR2 := MOV_RX_refRY("00", "10")
	movR0RefR2Half := "0000 0000 " + movR0RefR2[len(movR0RefR2)/2:]

	cpu.r1Out = WordFromString(address1)
	cpu.r2Out = WordFromString(address2)

	movR0R1Val := WordFromString(movR0RefR1)
	movR0R2Val := WordFromString(movR0RefR2)
	ramIn := Memory{
		0: movR0R1Val.FirstByte(),
		1: movR0R1Val.SecondByte(),
		2: movR0R2Val.FirstByte(),
		3: movR0R2Val.SecondByte(),
		4: value1val.FirstByte(),
		5: value2val.FirstByte(),
	}
	cpu.ramOut = ramIn

	var fsmOut [6]string
	var pcOut [5]string
	for i := 0; i < 6; i++ {
		fsmOut[i] = utils.NumToBinString(i, wordSize)
	}
	for i := 0; i < 5; i++ {
		pcOut[i] = utils.NumToBinString(i, wordSize)
	}

	testData := []*Data{
		NewData(movR0RefR1Half, movR0RefR1Half, fsmOut[1], pcOut[1], emptyWord,
			emptyWord, address1, address2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movR0RefR1, emptyWord, fsmOut[2], pcOut[2], emptyWord,
			emptyWord, address1, address2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movR0RefR1, emptyWord, fsmOut[3], pcOut[2], address1,
			emptyWord, address1, address2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movR0RefR1, value1, fsmOut[4], pcOut[2], address1,
			value1, address1, address2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movR0RefR1, emptyWord, fsmOut[5], pcOut[2], address1,
			value1, address1, address2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movR0RefR1, emptyWord, fsmOut[0], pcOut[2], address1,
			value1, address1, address2, emptyWord).WithKeyValue("memory", ramIn),

		NewData(movR0RefR2Half, movR0RefR2Half, fsmOut[1], pcOut[3], address1,
			value1, address1, address2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movR0RefR2, emptyWord, fsmOut[2], pcOut[4], address1,
			value1, address1, address2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movR0RefR2, emptyWord, fsmOut[3], pcOut[4], address2,
			value1, address1, address2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movR0RefR2, value2, fsmOut[4], pcOut[4], address2,
			value2, address1, address2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movR0RefR2, emptyWord, fsmOut[5], pcOut[4], address2,
			value2, address1, address2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movR0RefR2, emptyWord, fsmOut[0], pcOut[4], address2,
			value2, address1, address2, emptyWord).WithKeyValue("memory", ramIn),
	}

	for _, data := range testData {
		cpu.step(true)
		// fmt.Println("FSM: ", cpu.fsmOut, ", PC: ", cpu.pcOut)
		assert.Equal(t, data.NextWord(), cpu.irOut)
		assert.Equal(t, data.NextWord(), cpu.busOut)
		assert.Equal(t, data.NextWord(), cpu.fsmOut)
		assert.Equal(t, data.NextWord(), cpu.pcOut)
		assert.Equal(t, data.NextWord(), cpu.addressOut)

		assert.Equal(t, data.NextWord(), cpu.r0Out)
		assert.Equal(t, data.NextWord(), cpu.r1Out)
		assert.Equal(t, data.NextWord(), cpu.r2Out)
		assert.Equal(t, data.NextWord(), cpu.r3Out)

		assert.Equal(t, data.GetMemoryValue("memory"), cpu.ramOut)
	}
}

func Test_CPU_MovRefRxRy(t *testing.T) {
	cpu := new(CPU)
	address := "0000 0000 0000 0100"
	value1 := "0001 0000 1000 0001"
	value2 := "0000 0000 1011 0111"
	value1val := WordFromString(value1)
	value2val := WordFromString(value2)

	movRefR0R1 := MOV_refRX_RY("00", "01")
	movRefR0R1Half := "0000 0000 " + movRefR0R1[len(movRefR0R1)/2:]
	movRefR0R2 := MOV_refRX_RY("00", "10")
	movRefR0R2Half := "0000 0000 " + movRefR0R2[len(movRefR0R2)/2:]

	cpu.r0Out = WordFromString(address)
	cpu.r1Out = WordFromString(value1)
	cpu.r2Out = WordFromString(value2)

	movR0R1Val := WordFromString(movRefR0R1)
	movR0R2Val := WordFromString(movRefR0R2)
	ramIn := Memory{
		0: movR0R1Val.FirstByte(),
		1: movR0R1Val.SecondByte(),
		2: movR0R2Val.FirstByte(),
		3: movR0R2Val.SecondByte(),
		4: Byte{},
	}
	cpu.ramOut = ramIn

	ramOut1 := Memory{
		0: movR0R1Val.FirstByte(),
		1: movR0R1Val.SecondByte(),
		2: movR0R2Val.FirstByte(),
		3: movR0R2Val.SecondByte(),
		4: value1val.FirstByte(),
	}

	ramOut2 := Memory{
		0: movR0R1Val.FirstByte(),
		1: movR0R1Val.SecondByte(),
		2: movR0R2Val.FirstByte(),
		3: movR0R2Val.SecondByte(),
		4: value2val.FirstByte(),
	}

	var fsmOut [6]string
	var pcOut [5]string
	for i := 0; i < 6; i++ {
		fsmOut[i] = utils.NumToBinString(i, wordSize)
	}
	for i := 0; i < 5; i++ {
		pcOut[i] = utils.NumToBinString(i, wordSize)
	}

	testData := []*Data{
		NewData(movRefR0R1Half, movRefR0R1Half, fsmOut[1], pcOut[1], emptyWord,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movRefR0R1, emptyWord, fsmOut[2], pcOut[2], emptyWord,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movRefR0R1, address, fsmOut[3], pcOut[2], address,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movRefR0R1, value1, fsmOut[4], pcOut[2], address,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramOut1),
		NewData(movRefR0R1, emptyWord, fsmOut[5], pcOut[2], address,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramOut1),
		NewData(movRefR0R1, emptyWord, fsmOut[0], pcOut[2], address,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramOut1),

		NewData(movRefR0R2Half, movRefR0R2Half, fsmOut[1], pcOut[3], address,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movRefR0R2, emptyWord, fsmOut[2], pcOut[4], address,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movRefR0R2, address, fsmOut[3], pcOut[4], address,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramIn),
		NewData(movRefR0R2, value2, fsmOut[4], pcOut[4], address,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramOut2),
		NewData(movRefR0R2, emptyWord, fsmOut[5], pcOut[4], address,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramOut2),
		NewData(movRefR0R2, emptyWord, fsmOut[0], pcOut[4], address,
			address, value1, value2, emptyWord).WithKeyValue("memory", ramOut2),
	}

	for _, data := range testData {
		cpu.step(true)
		// fmt.Println("FSM: ", cpu.fsmOut, ", PC: ", cpu.pcOut)
		assert.Equal(t, data.NextWord(), cpu.irOut)
		assert.Equal(t, data.NextWord(), cpu.busOut)
		assert.Equal(t, data.NextWord(), cpu.fsmOut)
		assert.Equal(t, data.NextWord(), cpu.pcOut)
		assert.Equal(t, data.NextWord(), cpu.addressOut)

		assert.Equal(t, data.NextWord(), cpu.r0Out)
		assert.Equal(t, data.NextWord(), cpu.r1Out)
		assert.Equal(t, data.NextWord(), cpu.r2Out)
		assert.Equal(t, data.NextWord(), cpu.r3Out)

		assert.Equal(t, data.GetMemoryValue("memory"), cpu.ramOut)
	}
}

func Test_CPU_MovRxValue(t *testing.T) {
	cpu := new(CPU)
	value1 := "0010 1000 1000 0010"
	value1Half1 := "0000 0000 " + value1[len(value1)/2:]
	value1Half2 := "0000 0000" + value1[:len(value1)/2]
	value2 := "1000 0000 1010 0010"
	value2Half1 := "0000 0000 " + value2[len(value2)/2:]
	// value2Half2WithPrev := movR0Value2[:len(movR0Value2)/2] + value1[len(value2)/2:]

	value2Half2 := "0000 0000" + value2[:len(value2)/2]
	movR0Value1 := MOV_RX_value("00")
	movR0Value1Half := "0000 0000 " + movR0Value1[len(movR0Value1)/2:]
	movR0Value2 := MOV_RX_value("01")
	movR0Value2HalfWithPrev := value1[:len(value1)/2] + movR0Value2[len(movR0Value2)/2:]
	movR0Value2Half := "0000 0000 " + movR0Value2[len(movR0Value2)/2:]

	value1Val := WordFromString(value1)
	value2Val := WordFromString(value2)
	movR0Value1Val := WordFromString(movR0Value1)
	movR0Value2Val := WordFromString(movR0Value2)
	ramOut := Memory{
		0: movR0Value1Val.FirstByte(),
		1: movR0Value1Val.SecondByte(),
		2: value1Val.FirstByte(),
		3: value1Val.SecondByte(),
		4: movR0Value2Val.FirstByte(),
		5: movR0Value2Val.SecondByte(),
		6: value2Val.FirstByte(),
		7: value2Val.SecondByte(),
		8: Byte{},
	}
	cpu.ramOut = ramOut

	var fsmOut [6]string
	var pcOut [10]string
	for i := 0; i < 6; i++ {
		fsmOut[i] = utils.NumToBinString(i, wordSize)
	}
	for i := 0; i < len(pcOut); i++ {
		pcOut[i] = utils.NumToBinString(i, wordSize)
	}

	testData := []*Data{
		NewData(movR0Value1Half, movR0Value1Half, fsmOut[1], pcOut[1], "0",
			emptyWord, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0Value1, emptyWord, fsmOut[2], pcOut[2], "1",
			emptyWord, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0Value1, emptyWord, fsmOut[0], pcOut[2], "1",
			emptyWord, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value1Half1, value1Half1, fsmOut[1], pcOut[3], "1",
			emptyWord, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value1, value1Half2, fsmOut[2], pcOut[4], "1",
			emptyWord, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value1, emptyWord, fsmOut[3], pcOut[4], "1",
			emptyWord, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value1, emptyWord, fsmOut[4], pcOut[4], "1",
			emptyWord, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value1, value1, fsmOut[5], pcOut[4], "1",
			value1, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value1, emptyWord, fsmOut[0], pcOut[4], "0",
			value1, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),

		NewData(movR0Value2HalfWithPrev, movR0Value2Half, fsmOut[1], pcOut[5], "0",
			value1, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0Value2, emptyWord, fsmOut[2], pcOut[6], "1",
			value1, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(movR0Value2, emptyWord, fsmOut[0], pcOut[6], "1",
			value1, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value2Half1, value2Half1, fsmOut[1], pcOut[7], "1",
			value1, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value2, value2Half2, fsmOut[2], pcOut[8], "1",
			value1, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value2, emptyWord, fsmOut[3], pcOut[8], "1",
			value1, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value2, value1, fsmOut[4], pcOut[8], "1",
			value1, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value2, value2, fsmOut[5], pcOut[8], "1",
			value2, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
		NewData(value2, emptyWord, fsmOut[0], pcOut[8], "0",
			value2, emptyWord, emptyWord, emptyWord).WithKeyValue("memory", ramOut),
	}

	for _, data := range testData {
		cpu.step(true)
		// fmt.Println("FSM: ", cpu.fsmOut, ", PC: ", cpu.pcOut)
		assert.Equal(t, data.NextWord(), cpu.irOut)
		assert.Equal(t, data.NextWord(), cpu.busOut)
		assert.Equal(t, data.NextWord(), cpu.fsmOut)
		assert.Equal(t, data.NextWord(), cpu.pcOut)
		assert.Equal(t, data.NextBool(), cpu.valueBitOut)

		assert.Equal(t, data.NextWord(), cpu.r0Out)
		assert.Equal(t, data.NextWord(), cpu.r1Out)
		assert.Equal(t, data.NextWord(), cpu.r2Out)
		assert.Equal(t, data.NextWord(), cpu.r3Out)

		assert.Equal(t, data.GetMemoryValue("memory"), cpu.ramOut)
	}
}

func Test_CPU_AddRxRy(t *testing.T) {
	cpu := new(CPU)
	value0 := "0001 0000 1000 0001"
	value1 := "0000 0000 1000 0010"
	sumValue := "0001 0001 0000 0011"
	addR0R1 := ADD_RX_RY("00", "01")

	data := NewData(value0, value1, addR0R1)

	cpu.r0Out = data.NextWord()
	cpu.r1Out = data.NextWord()

	addR0R1Val := data.NextWord()
	ramOut := Memory{
		0: addR0R1Val.FirstByte(),
		1: addR0R1Val.SecondByte(),
	}
	cpu.ramOut = ramOut

	var fsmOut [5]string
	var pcOut [5]string
	for i := 0; i < 5; i++ {
		fsmOut[i] = utils.NumToBinString(i, wordSize)
	}
	for i := 0; i < 5; i++ {
		pcOut[i] = utils.NumToBinString(i, wordSize)
	}

	testData := []*Data{
		NewData(emptyWord, emptyWord, emptyWord,
			value0, value1, emptyWord, emptyWord),
		NewData(value1, value1, emptyWord,
			value0, value1, emptyWord, emptyWord),
		NewData(value0, value1, sumValue,
			value0, value1, emptyWord, emptyWord),
		NewData(sumValue, value1, sumValue,
			sumValue, value1, emptyWord, emptyWord),
	}

	cpu.step(true)
	for _, data := range testData {
		info := fmt.Sprintf("FSM: %s, PC: %s", cpu.fsmOut, cpu.pcOut)
		cpu.step(true)

		assert.Equal(t, data.NextWord(), cpu.busOut, info)
		assert.Equal(t, data.NextWord(), cpu.tmpOut)
		assert.Equal(t, data.NextWord(), cpu.accOut)

		assert.Equal(t, data.NextWord(), cpu.r0Out)
		assert.Equal(t, data.NextWord(), cpu.r1Out)
		assert.Equal(t, data.NextWord(), cpu.r2Out)
		assert.Equal(t, data.NextWord(), cpu.r3Out)
	}
}

func Test_CPU_NotRx(t *testing.T) {
	cpu := new(CPU)
	value := "0001 0011 1010 0001"
	notValue := "1110 1100 0101 1110"
	notR0R1 := NOT_RX("00")

	data := NewData(value, notR0R1)

	cpu.r0Out = data.NextWord()

	addR0R1Val := data.NextWord()
	ramOut := Memory{
		0: addR0R1Val.FirstByte(),
		1: addR0R1Val.SecondByte(),
	}
	cpu.ramOut = ramOut

	testData := []*Data{
		NewData(emptyWord, emptyWord, emptyWord,
			value, emptyWord, emptyWord, emptyWord),
		NewData(value, value, emptyWord,
			value, emptyWord, emptyWord, emptyWord),
		NewData(value, value, notValue,
			value, emptyWord, emptyWord, emptyWord),
		NewData(notValue, value, notValue,
			notValue, emptyWord, emptyWord, emptyWord),
	}

	cpu.step(true)
	for _, data := range testData {
		info := fmt.Sprintf("FSM: %s, PC: %s", cpu.fsmOut, cpu.pcOut)
		cpu.step(true)

		assert.Equal(t, data.NextWord(), cpu.busOut, info)
		assert.Equal(t, data.NextWord(), cpu.tmpOut)
		assert.Equal(t, data.NextWord(), cpu.accOut, info)

		assert.Equal(t, data.NextWord(), cpu.r0Out)
		assert.Equal(t, data.NextWord(), cpu.r1Out)
		assert.Equal(t, data.NextWord(), cpu.r2Out)
		assert.Equal(t, data.NextWord(), cpu.r3Out)
	}
}
