package component

import (
	"computer-simulator/pkg/utils"
	"strconv"
)

type MuxInputPins struct {
	A   bool
	B   bool
	Sel bool
}

type Mux16InputPins struct {
	A   Word
	B   Word
	Sel bool
}

type Mux4WayInputPins struct {
	A   bool
	B   bool
	C   bool
	D   bool
	Sel [2]bool
}

type Mux4Way16InputPins struct {
	A   Word
	B   Word
	C   Word
	D   Word
	Sel [2]bool
}

type Mux8WayInputPins struct {
	A   bool
	B   bool
	C   bool
	D   bool
	E   bool
	F   bool
	G   bool
	H   bool
	Sel [3]bool
}

type Mux8Way16InputPins struct {
	A   Word
	B   Word
	C   Word
	D   Word
	E   Word
	F   Word
	G   Word
	H   Word
	Sel [3]bool
}

type DMuxInputPins struct {
	In  bool
	Sel bool
}

type DMux16InputPins struct {
	In  Word
	Sel bool
}

type DMux4WayInputPins struct {
	In  bool
	Sel [2]bool
}

type DMux4Way16InputPins struct {
	In  Word
	Sel [2]bool
}

type DMux8WayInputPins struct {
	In  bool
	Sel [3]bool
}

type DMux8Way16InputPins struct {
	In  Word
	Sel [3]bool
}

type DMux256WayInputPins struct {
	In  bool
	Sel Byte
}

type DMux4WayOutPins struct {
	A bool
	B bool
	C bool
	D bool
}

type DMux4Way16OutPins struct {
	A Word
	B Word
	C Word
	D Word
}

type DMux8WayOutPins struct {
	A bool
	B bool
	C bool
	D bool
	E bool
	F bool
	G bool
	H bool
}

type DMux8Way16OutPins struct {
	A Word
	B Word
	C Word
	D Word
	E Word
	F Word
	G Word
	H Word
}

type DMux256WayOutPins struct {
	Out [256]bool
}

func make16Mux(f interface{}, input interface{}) *Out16Pins {
	var out Out16Pins

	for i := 0; i < wordSize; i++ {
		switch f := f.(type) {
		case func(input *MuxInputPins) *OutPins:
			assertedInput := input.(*Mux16InputPins)
			out.Out[i] = f(&MuxInputPins{
				A:   assertedInput.A[i],
				B:   assertedInput.B[i],
				Sel: assertedInput.Sel,
			}).Out
		case func(input *Mux4WayInputPins) *OutPins:
			assertedInput := input.(*Mux4Way16InputPins)
			out.Out[i] = f(&Mux4WayInputPins{
				A:   assertedInput.A[i],
				B:   assertedInput.B[i],
				C:   assertedInput.C[i],
				D:   assertedInput.D[i],
				Sel: assertedInput.Sel,
			}).Out
		case func(input *Mux8WayInputPins) *OutPins:
			assertedInput := input.(*Mux8Way16InputPins)
			out.Out[i] = f(&Mux8WayInputPins{
				A:   assertedInput.A[i],
				B:   assertedInput.B[i],
				C:   assertedInput.C[i],
				D:   assertedInput.D[i],
				E:   assertedInput.E[i],
				F:   assertedInput.F[i],
				G:   assertedInput.G[i],
				H:   assertedInput.H[i],
				Sel: assertedInput.Sel,
			}).Out
		}
	}

	return &out
}

func MUX(input *MuxInputPins) *OutPins {
	notIn := NOT(&InPins{input.Sel})

	and1 := AND(&ABPins{input.A, notIn.Out})
	and2 := AND(&ABPins{input.B, input.Sel})
	or := OR(&ABPins{and1.Out, and2.Out})

	return or
}

func MUX16(input *Mux16InputPins) *Out16Pins {
	return make16Mux(MUX, input)
}

func MUX4Way(input *Mux4WayInputPins) *OutPins {
	mux1 := MUX(&MuxInputPins{
		A:   input.A,
		B:   input.B,
		Sel: input.Sel[0],
	})

	mux2 := MUX(&MuxInputPins{
		A:   input.C,
		B:   input.D,
		Sel: input.Sel[0],
	})

	mux3 := MUX(&MuxInputPins{
		A:   mux1.Out,
		B:   mux2.Out,
		Sel: input.Sel[1],
	})

	return mux3
}

func MUX4Way16(input *Mux4Way16InputPins) *Out16Pins {
	return make16Mux(MUX4Way, input)
}

func MUX8Way(input *Mux8WayInputPins) *OutPins {
	sel := [2]bool{input.Sel[0], input.Sel[1]}

	mux1 := MUX4Way(&Mux4WayInputPins{
		A:   input.A,
		B:   input.B,
		C:   input.C,
		D:   input.D,
		Sel: sel,
	})

	mux2 := MUX4Way(&Mux4WayInputPins{
		A:   input.E,
		B:   input.F,
		C:   input.G,
		D:   input.H,
		Sel: sel,
	})

	mux3 := MUX(&MuxInputPins{
		A:   mux1.Out,
		B:   mux2.Out,
		Sel: input.Sel[2],
	})

	return mux3
}

func MUX8Way16(input *Mux8Way16InputPins) *Out16Pins {
	return make16Mux(MUX8Way, input)
}

func make16DMux(f interface{}, input interface{}) interface{} {

	switch f := f.(type) {
	case func(input *DMuxInputPins) *ABPins:
		var out16 AB16Pins
		assertedInput := input.(*DMux16InputPins)
		for i := 0; i < wordSize; i++ {
			out := f(&DMuxInputPins{
				In:  assertedInput.In[i],
				Sel: assertedInput.Sel,
			})
			out16.A[i] = out.A
			out16.B[i] = out.B
		}
		return &out16
	case func(input *DMux4WayInputPins) *DMux4WayOutPins:
		var out16 DMux4Way16OutPins
		assertedInput := input.(*DMux4Way16InputPins)
		for i := 0; i < wordSize; i++ {
			out := f(&DMux4WayInputPins{
				In:  assertedInput.In[i],
				Sel: assertedInput.Sel,
			})
			out16.A[i] = out.A
			out16.B[i] = out.B
			out16.C[i] = out.C
			out16.D[i] = out.D
		}
		return &out16
	case func(input *DMux8WayInputPins) *DMux8WayOutPins:
		var out16 DMux8Way16OutPins
		assertedInput := input.(*DMux8Way16InputPins)
		for i := 0; i < wordSize; i++ {
			out := f(&DMux8WayInputPins{
				In:  assertedInput.In[i],
				Sel: assertedInput.Sel,
			})
			out16.A[i] = out.A
			out16.B[i] = out.B
			out16.C[i] = out.C
			out16.D[i] = out.D

			out16.E[i] = out.E
			out16.F[i] = out.F
			out16.G[i] = out.G
			out16.H[i] = out.H
		}
		return &out16
	}
	return nil
}

func DMUX(input *DMuxInputPins) *ABPins {
	notIn := NOT(&InPins{input.Sel})

	and1 := AND(&ABPins{input.In, notIn.Out})
	and2 := AND(&ABPins{input.In, input.Sel})

	return &ABPins{
		A: and1.Out,
		B: and2.Out,
	}
}

func DMUX16(input *DMux16InputPins) *AB16Pins {
	return make16DMux(DMUX, input).(*AB16Pins)
}

func DMUX4Way(input *DMux4WayInputPins) *DMux4WayOutPins {
	dmux1 := DMUX(&DMuxInputPins{
		In:  input.In,
		Sel: input.Sel[1],
	})

	dmux2 := DMUX(&DMuxInputPins{
		In:  dmux1.A,
		Sel: input.Sel[0],
	})

	dmux3 := DMUX(&DMuxInputPins{
		In:  dmux1.B,
		Sel: input.Sel[0],
	})

	return &DMux4WayOutPins{
		A: dmux2.A,
		B: dmux2.B,
		C: dmux3.A,
		D: dmux3.B,
	}
}

func DMUX4Way16(input *DMux4Way16InputPins) *DMux4Way16OutPins {
	return make16DMux(DMUX4Way, input).(*DMux4Way16OutPins)
}

func DMUX8Way(input *DMux8WayInputPins) *DMux8WayOutPins {
	dmux1 := DMUX(&DMuxInputPins{
		In:  input.In,
		Sel: input.Sel[2],
	})

	sel := [2]bool{input.Sel[0], input.Sel[1]}
	dmux2 := DMUX4Way(&DMux4WayInputPins{
		In:  dmux1.A,
		Sel: sel,
	})

	dmux3 := DMUX4Way(&DMux4WayInputPins{
		In:  dmux1.B,
		Sel: sel,
	})

	return &DMux8WayOutPins{
		A: dmux2.A,
		B: dmux2.B,
		C: dmux2.C,
		D: dmux2.D,

		E: dmux3.A,
		F: dmux3.B,
		G: dmux3.C,
		H: dmux3.D,
	}
}

func DMUX8Way16(input *DMux8Way16InputPins) *DMux8Way16OutPins {
	return make16DMux(DMUX8Way, input).(*DMux8Way16OutPins)
}

func DMUX256Way(input *DMux256WayInputPins) *DMux256WayOutPins {
	var out DMux256WayOutPins

	selInt, _ := strconv.ParseInt(utils.BinToString(input.Sel[:]), 2, 64)
	out.Out[selInt] = input.In

	return &out
}
