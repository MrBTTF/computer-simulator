package component

import (
	"computer-simulator/pkg/utils"
	"math"
	"math/rand"
	"testing"

	"github.com/stretchr/testify/assert"
)

func Test_DFF(t *testing.T) {
	testData := []*Data{
		NewData("0", "0", "0",
			"0", "0"),
		NewData("0", "1", "0",
			"0", "0"),
		NewData("1", "0", "0",
			"0", "0"),
		NewData("1", "1", "0",
			"1", "0"),

		NewData("0", "0", "1",
			"1", "1"),
		NewData("0", "1", "1",
			"0", "1"),
		NewData("1", "0", "1",
			"1", "1"),
		NewData("1", "1", "1",
			"1", "1"),
	}
	for _, data := range testData {
		input := &DFFInputPins{
			In:      data.NextBool(),
			ClockIn: data.NextBool(),
			Out:     data.NextBool(),
		}

		expected := MemOutPins{
			NextOut: data.NextBool(),
			Out:     data.NextBool(),
		}

		result := *DFF(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_BIT(t *testing.T) {
	testData := []*Data{
		NewData("0", "0", "0", "1", "0",
			"0", "0"),
		NewData("0", "0", "1", "1", "0",
			"0", "0"),
		NewData("0", "1", "0", "1", "0",
			"0", "0"),
		NewData("0", "1", "1", "1", "0",
			"0", "0"),

		NewData("1", "0", "0", "1", "0",
			"0", "0"),
		NewData("1", "0", "1", "1", "0",
			"0", "0"),
		NewData("1", "1", "0", "1", "0",
			"0", "0"),
		NewData("1", "1", "1", "1", "0",
			"1", "0"),

		NewData("0", "0", "0", "1", "1",
			"1", "1"),
		NewData("0", "0", "1", "1", "1",
			"1", "1"),
		NewData("0", "1", "0", "1", "1",
			"1", "1"),
		NewData("0", "1", "1", "1", "1",
			"0", "1"),

		NewData("1", "0", "0", "1", "1",
			"1", "1"),
		NewData("1", "0", "1", "1", "1",
			"1", "1"),
		NewData("1", "1", "0", "1", "1",
			"1", "1"),
		NewData("1", "1", "1", "1", "1",
			"1", "1"),

		NewData("1", "0", "0", "0", "1",
			"1", "0"),
		NewData("1", "0", "1", "0", "1",
			"1", "0"),
		NewData("1", "1", "0", "0", "1",
			"1", "0"),
		NewData("1", "1", "1", "0", "1",
			"1", "0"),
	}
	for _, data := range testData {
		input := &BitInputPins{
			In:      data.NextBool(),
			ClockIn: data.NextBool(),
			Load:    data.NextBool(),
			Enable:  data.NextBool(),
			Out:     data.NextBool(),
		}

		expected := MemOutPins{
			NextOut: data.NextBool(),
			Out:     data.NextBool(),
		}

		result := *BIT(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_REG16(t *testing.T) {
	var testData []*Data
	for i := 0; i < wordSize; i++ {
		value := utils.NumToBinString(i, wordSize)
		value2 := utils.NumToBinString(wordSize-1-i, wordSize)

		testData = append(testData, []*Data{
			NewData(value, "0", "0", "1", value2,
				value2, value2),
			NewData(value, "0", "1", "1", value2,
				value2, value2),
			NewData(value, "1", "0", "1", value2,
				value2, value2),
			NewData(value, "1", "1", "1", value2,
				value, value2),

			NewData(value, "0", "0", "0", value2,
				value2, "0000 0000 0000 0000"),
		}...)
	}

	for _, data := range testData {
		input := &Reg16InputPins{
			In:      data.NextWord(),
			ClockIn: data.NextBool(),
			Load:    data.NextBool(),
			Enable:  data.NextBool(),
			Out:     data.NextWord(),
		}

		expected := MemOut16Pins{
			NextOut: data.NextWord(),
			Out:     data.NextWord(),
		}

		result := *REG16(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_REG2Bytes(t *testing.T) {
	testData := []*Data{
		NewData("0000 1100", "1", "1", "0", "1", "0000 0000 0000 0000",
			"0000 0000 0000 1100", "0000 0000 0000 0000"),
		NewData("0000 1100", "1", "0", "1", "1", "0000 0000 0000 0000",
			"0000 1100 0000 0000", "0000 0000 0000 0000"),
	}

	for _, data := range testData {
		input := &Reg2BytesInputPins{
			In:      data.NextByte(),
			ClockIn: data.NextBool(),
			Load0:   data.NextBool(),
			Load1:   data.NextBool(),
			Enable:  data.NextBool(),
			Out:     data.NextWord(),
		}

		expected := MemOut16Pins{
			NextOut: data.NextWord(),
			Out:     data.NextWord(),
		}

		result := *REG2Bytes(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}

func Test_RAM16K(t *testing.T) {
	log2RamSize := int(math.Log2(float64(ram16KSize)))

	addresses := [3]int{0, int(rand.Int31n(ram16KSize - 1)), ram16KSize - 1}

	value1 := utils.NumToBinString(int(rand.Int31n(int32(log2RamSize))), byteSize)
	value2 := utils.NumToBinString(log2RamSize-1-int(rand.Int31n(int32(log2RamSize))), byteSize)

	out := make(Memory)
	var v1, v2 Byte
	copy(v1[:], utils.StringToBin(value1, wordSize))
	copy(v2[:], utils.StringToBin(value2, wordSize))

	var testData []*Data
	for _, address := range addresses {
		addressStr := utils.NumToBinString(address, adressSize)

		testData = append(testData, []*Data{
			NewData(value1, "0", "0", addressStr, "1",
				value2).WithKeyValue("address", address).WithKeyValue("value", v2),
			NewData(value1, "0", "1", addressStr, "1",
				value2).WithKeyValue("address", address).WithKeyValue("value", v2),
			NewData(value1, "1", "0", addressStr, "1",
				value2).WithKeyValue("address", address).WithKeyValue("value", v2),
			NewData(value1, "1", "1", addressStr, "1",
				value1).WithKeyValue("address", address).WithKeyValue("value", v1),
		}...)
	}

	for _, data := range testData {
		out[data.GetIntValue("address")] = v2

		input := &RAM16KInputPins{
			In:      data.NextByte(),
			ClockIn: data.NextBool(),
			Load:    data.NextBool(),
			Address: data.NextAddress(),
			Enable:  data.NextBool(),
			Out:     out,
		}

		out[data.GetIntValue("address")] = data.GetByteValue("value")

		expected := RAM16KOutPins{
			Out:       data.NextByte(),
			MemoryOut: out,
		}

		result := *RAM16K(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}

}

func Test_PC(t *testing.T) {
	testData := []*Data{
		NewData("0000 0000 0000 0000", "0", "0", "0", "0", "0000 0000 0000 0000",
			"0000 0000 0000 0000", "0000 0000 0000 0000"),
		NewData("0000 0000 0000 1111", "1", "1", "0", "0", "0000 0000 0000 0000",
			"0000 0000 0000 1111", "0000 0000 0000 0000"),
		NewData("0000 0000 0000 0000", "0", "0", "0", "0", "0000 0000 0000 1111",
			"0000 0000 0000 1111", "0000 0000 0000 1111"),

		NewData("0000 0000 0000 1111", "0", "0", "1", "0", "0000 0000 0000 0001",
			"0000 0000 0000 0001", "0000 0000 0000 0001"),

		NewData("0000 0000 0000 1111", "1", "0", "1", "0", "0000 0000 0000 0000",
			"0000 0000 0000 0001", "0000 0000 0000 0000"),

		NewData("0000 0000 0000 1111", "1", "0", "1", "1", "0000 1111 0000 0000",
			"0000 0000 0000 0000", "0000 1111 0000 0000"),
	}
	for _, data := range testData {
		input := &PCInputPins{
			In:      data.NextWord(),
			ClockIn: data.NextBool(),
			Load:    data.NextBool(),
			Inc:     data.NextBool(),
			Reset:   data.NextBool(),
			Out:     data.NextWord(),
		}

		expected := MemOut16Pins{
			NextOut: data.NextWord(),
			Out:     data.NextWord(),
		}

		result := *PC(input)
		assert.Equal(t, expected, result, "input: %v", input)
	}
}
