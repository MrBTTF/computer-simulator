package component_old

import (
	"computer-simulator/pkg/utils"
	"strconv"
	"testing"

	"github.com/stretchr/testify/assert"
)

func loadByte(cpu *CPU, address int, data string) {
	dataBin := utils.StringToBin(data, 8)
	addressBin := utils.StringToBin(utils.NumToBinString(address, 14), 14)

	cpu.ram.SetInput("clockIn", []bool{true})
	cpu.ram.SetInput("load", []bool{true})
	cpu.ram.SetInput("in", dataBin)
	cpu.ram.SetInput("address", addressBin)
	cpu.ram.GetOutput("out")

	cpu.ram.SetInput("load", []bool{false})
}

func loadInstruction(cpu *CPU, address int, instruction string) {
	loadByte(cpu, address, instruction[8:])
	loadByte(cpu, address+1, instruction[:8])
}

func loadInstructions(cpu *CPU, instructions []string) {
	for i, instruction := range instructions {
		loadInstruction(cpu, i*2, instruction)
	}
}

func loadInstructionsAt(cpu *CPU, address int, instructions []string) {
	for _, instruction := range instructions {
		loadInstruction(cpu, address, instruction)
	}
}

func tick(cpu *CPU, count int) {
	for i := 0; i < count; i++ {
		cpu.step([]bool{true})
		cpu.step([]bool{false})
	}
}

func Test_FSM(t *testing.T) {
	testSet := TestSet{
		inputsNames:  []string{"inc", "reset", "clockIn"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"001", "000"},
			{"100", "000"},
			{"101", "001"},
			{"100", "001"},
			{"101", "010"},
			{"100", "010"},
			{"101", "011"},
			{"100", "011"},
			{"101", "100"},
			{"100", "100"},
			{"101", "000"},
			{"100", "000"},
			{"111", "001"},
			{"110", "001"},
			{"101", "000"},
			{"100", "000"},
			{"101", "001"},
			{"100", "001"},
		},
	}

	c := FSM.WithSize(3)
	checkComponent(t, c, testSet)
}

func Test_TwoByteRegister(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in", "load1", "load0", "clockIn"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"00 001", "00"},
			{"11 011", "01"},
			{"10 011", "00"},
			{"11 101", "10"},
			{"00 111", "00"},
			{"11 111", "11"},
		},
	}
	c := TwoByteRegister.WithSize(2)
	checkComponent(t, c, testSet)
}

func TestCPU_FetchExecute(t *testing.T) {
	cpu := NewCPU()

	instruction := "0100110110110111"
	loadInstruction(cpu, 0, instruction)

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, "001", utils.BinToString(cpu.fsm.GetOutput("out")))
	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, instruction, utils.BinToString(cpu.insRegister.GetOutput("out")))
	assert.Equal(t, "010", utils.BinToString(cpu.fsm.GetOutput("out")))
	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, "011", utils.BinToString(cpu.fsm.GetOutput("out")))
	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, "100", utils.BinToString(cpu.fsm.GetOutput("out")))
	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, "001", utils.BinToString(cpu.fsm.GetOutput("out")))
}

func TestCPU_MOV_RX_RY(t *testing.T) {
	cpu := NewCPU()

	instructions := []string{
		"0000000000000001", //MOV R0, R1
		"0000000000001000", //MOV R2, R0
		"0000000000001110", //MOV R3, R2
		"0000000000000111", //MOV R1, R3
	}
	data := utils.StringToBin("0100110110110111", 16)
	empty := utils.StringToBin("0000000000000000", 16)

	loadInstructions(cpu, instructions)

	cpu.registers[1].SetInput("clockIn", []bool{true})
	cpu.registers[1].SetInput("load", []bool{true})
	cpu.registers[1].SetInput("in", data)

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	cpu.step([]bool{true})
	cpu.step([]bool{false})

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, data, cpu.bus.GetOutput("out"))

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, data, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[3].GetOutput("out"))

	tick(cpu, 4)
	assert.Equal(t, data, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[3].GetOutput("out"))

	tick(cpu, 4)
	assert.Equal(t, data, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[3].GetOutput("out"))

	cpu.registers[1].SetInput("clockIn", []bool{true})
	cpu.registers[1].SetInput("load", []bool{true})
	cpu.registers[1].SetInput("in", make([]bool, 16))

	tick(cpu, 4)
	assert.Equal(t, data, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[3].GetOutput("out"))
}

func TestCPU_MOV_value(t *testing.T) {
	cpu := NewCPU()

	dataStr := "0100110110110111"
	instructions := []string{
		"0000000001000000", //MOV R0, value
		dataStr,
		"0000000001000100", //MOV R2, value
		dataStr,
		"0000000001001000", //MOV R3, value
		dataStr,
		"0000000001001100", //MOV R1, value
		dataStr,
	}
	data := utils.StringToBin(dataStr, 16)
	empty := utils.StringToBin("0000000000000000", 16)

	loadInstructions(cpu, instructions)

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, "001", utils.BinToString(cpu.fsm.GetOutput("out")))
	cpu.step([]bool{true})
	cpu.step([]bool{false})

	cpu.step([]bool{true})
	assert.Equal(t, "000", utils.BinToString(cpu.fsm.GetOutput("out")))
	cpu.step([]bool{false})
	cpu.step([]bool{true})
	assert.Equal(t, "001", utils.BinToString(cpu.fsm.GetOutput("out")))
	cpu.step([]bool{false})
	assert.Equal(t, data, cpu.valueRegister.GetOutput("out"))

	assert.Equal(t, "010", utils.BinToString(cpu.fsm.GetOutput("out")))
	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, data, cpu.bus.GetOutput("out"))

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, data, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[3].GetOutput("out"))

	tick(cpu, 6)
	assert.Equal(t, data, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[3].GetOutput("out"))

	tick(cpu, 6)
	assert.Equal(t, data, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[3].GetOutput("out"))

	tick(cpu, 6)
	assert.Equal(t, data, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[3].GetOutput("out"))
}

func TestCPU_MOV_RX_atRY(t *testing.T) {
	cpu := NewCPU()

	instructions := []string{
		"0000000000010001", //MOV R0, [R1]
		"0000000000011011", //MOV R2, [R3]
	}
	addressInt := 256
	addressStr := utils.NumToBinString(addressInt, 16)
	address := utils.StringToBin(addressStr, 16)
	dataStr := "0000000010110111"
	data := utils.StringToBin(dataStr, 16)
	empty := utils.StringToBin("0000000000000000", 16)

	loadInstructions(cpu, instructions)
	loadInstructionsAt(cpu, addressInt, []string{dataStr})

	cpu.registers[1].SetInput("clockIn", []bool{true})
	cpu.registers[1].SetInput("load", []bool{true})
	cpu.registers[1].SetInput("in", address)

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	cpu.step([]bool{true})
	cpu.step([]bool{false})

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, data, cpu.bus.GetOutput("out"))

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, data, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, address, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[3].GetOutput("out"))

	cpu.registers[3].SetInput("clockIn", []bool{true})
	cpu.registers[3].SetInput("load", []bool{true})
	cpu.registers[3].SetInput("in", address)

	tick(cpu, 4)
	assert.Equal(t, data, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, address, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, address, cpu.registers[3].GetOutput("out"))
}

func TestCPU_MOV_atRX_RY(t *testing.T) {
	cpu := NewCPU()

	instructions := []string{
		"0000000000100001", //MOV [R0], R1
		"0000000000101011", //MOV [R2], R3
	}
	addressInt := 256
	addressStr := utils.NumToBinString(addressInt, 16)
	address := utils.StringToBin(addressStr, 16)
	dataStr := "0000000010110111"
	data := utils.StringToBin(dataStr, 16)
	empty := utils.StringToBin("0000000000000000", 16)

	loadInstructions(cpu, instructions)

	ram, _ := cpu.ram.(*Component)

	cpu.registers[0].SetInput("clockIn", []bool{true})
	cpu.registers[0].SetInput("load", []bool{true})
	cpu.registers[0].SetInput("in", address)

	cpu.registers[1].SetInput("clockIn", []bool{true})
	cpu.registers[1].SetInput("load", []bool{true})
	cpu.registers[1].SetInput("in", data)

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	cpu.step([]bool{true})
	cpu.step([]bool{false})

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, data, cpu.bus.GetOutput("out"))

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, address, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[3].GetOutput("out"))

	assert.Equal(t, data[:cpu.byteSize], ram.components["register"+strconv.Itoa(addressInt)].GetOutput("out"))

	addressInt = 123
	addressStr = utils.NumToBinString(addressInt, 16)
	address = utils.StringToBin(addressStr, 16)
	cpu.registers[3].SetInput("clockIn", []bool{true})
	cpu.registers[3].SetInput("load", []bool{true})
	cpu.registers[3].SetInput("in", data)

	cpu.registers[2].SetInput("clockIn", []bool{true})
	cpu.registers[2].SetInput("load", []bool{true})
	cpu.registers[2].SetInput("in", address)

	cpu.step([]bool{true})
	cpu.step([]bool{false})

	cpu.step([]bool{true})
	cpu.step([]bool{false})

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, data, cpu.bus.GetOutput("out"))

	cpu.step([]bool{true})
	cpu.step([]bool{false})

	assert.Equal(t, address, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, data, cpu.registers[3].GetOutput("out"))

	assert.Equal(t, data[:cpu.byteSize], ram.components["register"+strconv.Itoa(addressInt)].GetOutput("out"))
}

func TestCPU_MOV_atRX_atRY(t *testing.T) {
	cpu := NewCPU()

	instructions := []string{
		"0000000000110001", //MOV [R0], [R1]
		"0000000000111011", //MOV [R2], [R3]
	}
	dataStr := "0000000010110111"
	data := utils.StringToBin(dataStr, 16)
	empty := utils.StringToBin("0000000000000000", 16)

	loadInstructions(cpu, instructions)

	ram, _ := cpu.ram.(*Component)

	addressIntTo := 256
	addressStr := utils.NumToBinString(addressIntTo, 16)
	addressTo := utils.StringToBin(addressStr, 16)
	cpu.registers[0].SetInput("clockIn", []bool{true})
	cpu.registers[0].SetInput("load", []bool{true})
	cpu.registers[0].SetInput("in", addressTo)

	addressIntFrom := 123
	addressStr = utils.NumToBinString(addressIntFrom, 16)
	addressFrom := utils.StringToBin(addressStr, 16)
	cpu.registers[1].SetInput("clockIn", []bool{true})
	cpu.registers[1].SetInput("load", []bool{true})
	cpu.registers[1].SetInput("in", addressFrom)

	loadInstructionsAt(cpu, addressIntFrom, []string{dataStr})

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	cpu.step([]bool{true})
	cpu.step([]bool{false})

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, data, cpu.bus.GetOutput("out"))

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, addressTo, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, addressFrom, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[3].GetOutput("out"))

	assert.Equal(t, data[:cpu.byteSize], ram.components["register"+strconv.Itoa(addressIntTo)].GetOutput("out"))

	addressIntTo = 432
	addressStr = utils.NumToBinString(addressIntTo, 16)
	addressTo = utils.StringToBin(addressStr, 16)
	cpu.registers[2].SetInput("clockIn", []bool{true})
	cpu.registers[2].SetInput("load", []bool{true})
	cpu.registers[2].SetInput("in", addressTo)

	addressIntFrom = 734
	addressStr = utils.NumToBinString(addressIntFrom, 16)
	addressFrom = utils.StringToBin(addressStr, 16)
	cpu.registers[3].SetInput("clockIn", []bool{true})
	cpu.registers[3].SetInput("load", []bool{true})
	cpu.registers[3].SetInput("in", addressFrom)

	loadInstructionsAt(cpu, addressIntFrom, []string{dataStr})

	cpu.step([]bool{true})
	cpu.step([]bool{false})

	cpu.step([]bool{true})
	cpu.step([]bool{false})

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, data, cpu.bus.GetOutput("out"))

	cpu.step([]bool{true})
	cpu.step([]bool{false})

	assert.Equal(t, addressTo, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, addressFrom, cpu.registers[3].GetOutput("out"))

	assert.Equal(t, data[:cpu.byteSize], ram.components["register"+strconv.Itoa(addressIntTo)].GetOutput("out"))
}

func TestCPU_ALU(t *testing.T) {
	cpu := NewCPU()

	instructions := []string{
		"0000000010000001", //ADD R0, R1
		"0000000010001000", //ADD R2, R0
	}
	op1 := utils.StringToBin("0000000110110111", 16)
	op2 := utils.StringToBin("0000000000000111", 16)
	result := utils.StringToBin("0000000110111110", 16)
	empty := utils.StringToBin("0000000000000000", 16)

	loadInstructions(cpu, instructions)

	cpu.registers[0].SetInput("clockIn", []bool{true})
	cpu.registers[0].SetInput("load", []bool{true})
	cpu.registers[0].SetInput("in", op1)

	cpu.registers[1].SetInput("clockIn", []bool{true})
	cpu.registers[1].SetInput("load", []bool{true})
	cpu.registers[1].SetInput("in", op2)

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	cpu.step([]bool{true})
	cpu.step([]bool{false})

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, op1, cpu.bus.GetOutput("out"))

	cpu.step([]bool{true})
	cpu.step([]bool{false})
	assert.Equal(t, result, cpu.registers[0].GetOutput("out"))
	assert.Equal(t, op2, cpu.registers[1].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[2].GetOutput("out"))
	assert.Equal(t, empty, cpu.registers[3].GetOutput("out"))

	tick(cpu, 4)
	// assert.Equal(t, data, cpu.registers[0].GetOutput("out"))
	// assert.Equal(t, data, cpu.registers[1].GetOutput("out"))
	// assert.Equal(t, data, cpu.registers[2].GetOutput("out"))
	// assert.Equal(t, empty, cpu.registers[3].GetOutput("out"))

}
