package component_old

var (
	ControlSection = NewComponent(calculateControlSection).
			WithBusInput("in").
			WithInputWithSize("flags", 4).
			WithInput("clockIn").
			WithOutput("outEnable").
			WithBusOutput("out").
			WithOutput("memEnable").
			WithOutput("memLoad").
			WithOutput("memAddress").
			WithOutput("memClockIn").
			WithOutput("TMPLoad").
			WithOutput("TMPClockIn").
			WithOutput("flagsLoad").
			WithOutput("flagsClockIn").
			WithOutputWithSize("op", 3).
			WithOutput("aluEnable")

	TwoByteRegister = NewComponent(calculateIR).
			WithBusInput("in").
			WithInput("load0").
			WithInput("load1").
			WithInput("clockIn").
			WithBusOutput("out")

	FSM = NewComponent(calculateFSM).
		WithInput("clockIn").
		WithInput("inc").
		WithInput("reset").
		WithBusOutput("out")
)

type CPU struct {
	busSize  uint
	byteSize uint

	registers     [4]IComponent
	ram           IComponent
	alu           IComponent
	tmp           IComponent
	flags         IComponent
	insRegister   IComponent
	pc            IComponent
	fsm           IComponent
	value         IComponent
	valueRegister IComponent

	bus IComponent

	run bool
}

func NewCPU() *CPU {
	cpu := new(CPU)
	cpu.busSize = 16
	cpu.byteSize = 8

	cpu.bus = Bus.WithSize(cpu.busSize)

	for i := range cpu.registers {
		cpu.registers[i] = Register.WithSize(cpu.busSize)
	}
	cpu.ram = RAM16K.WithSize(cpu.byteSize)
	cpu.alu = ALU.WithSize(cpu.busSize)
	cpu.tmp = Register.WithSize(cpu.busSize)
	cpu.flags = Register.WithSize(cpu.busSize)
	cpu.insRegister = TwoByteRegister.WithSize(cpu.busSize)
	cpu.pc = PC.WithSize(cpu.busSize)
	cpu.fsm = FSM.WithSize(3)
	cpu.value = Bit.Clone()
	cpu.valueRegister = TwoByteRegister.WithSize(cpu.busSize)

	return cpu
}

func (cpu *CPU) Run() {
	go func() {
		signal := true
		for cpu.run {
			cpu.step([]bool{signal})
			signal = !signal
		}
	}()
}

func readR(cpu *CPU, clockIn []bool, instruction []bool, second bool) []bool {

	cpu.registers[0].SetInput("clockIn", clockIn)
	cpu.registers[1].SetInput("clockIn", clockIn)
	cpu.registers[2].SetInput("clockIn", clockIn)
	cpu.registers[3].SetInput("clockIn", clockIn)

	regMux := Mux4Way.WithSize(cpu.busSize)
	regMux.SetInput("a", cpu.registers[0].GetOutput("out"))
	regMux.SetInput("b", cpu.registers[1].GetOutput("out"))
	regMux.SetInput("c", cpu.registers[2].GetOutput("out"))
	regMux.SetInput("d", cpu.registers[3].GetOutput("out"))

	if second {
		regMux.SetInput("sel", instruction[:2])
	} else {
		regMux.SetInput("sel", instruction[2:4])
	}

	return regMux.GetOutput("out")
}

func writeR(cpu *CPU, clockIn []bool, instruction []bool, load []bool) {

	regDmux := DMux4Way.WithSize(1)
	regDmux.SetInput("in", load)
	regDmux.SetInput("sel", cpu.insRegister.GetOutput("out")[2:4])

	cpu.registers[0].SetInput("load", regDmux.GetOutput("a"))
	cpu.registers[0].SetInput("in", cpu.bus.GetOutput("out"))
	cpu.registers[0].GetOutput("out")
	cpu.registers[1].SetInput("load", regDmux.GetOutput("b"))
	cpu.registers[1].SetInput("in", cpu.bus.GetOutput("out"))
	cpu.registers[1].GetOutput("out")
	cpu.registers[2].SetInput("load", regDmux.GetOutput("c"))
	cpu.registers[2].SetInput("in", cpu.bus.GetOutput("out"))
	cpu.registers[2].GetOutput("out")
	cpu.registers[3].SetInput("load", regDmux.GetOutput("d"))
	cpu.registers[3].SetInput("in", cpu.bus.GetOutput("out"))
	cpu.registers[3].GetOutput("out")
}

func movRXRYState2(cpu *CPU, clockIn []bool, instruction []bool, state2 []bool, controlBit []bool) {
	regOutput := readR(cpu, clockIn, instruction, true)

	andState2MOVrxry := AND(state2, controlBit)

	cpu.pc.SetInput("clockIn", clockIn)
	cpu.pc.SetInput("inc", andState2MOVrxry)
	cpu.pc.GetOutput("out")

	mux := MuxN.WithSize(cpu.busSize)
	mux.SetInput("a", cpu.bus.GetOutput("out"))
	mux.SetInput("b", regOutput)
	mux.SetInput("sel", andState2MOVrxry)
	cpu.bus.SetInput("in", mux.GetOutput("out"))
}

func movatRXRYState2(cpu *CPU, clockIn []bool, instruction []bool, state2 []bool, controlBit []bool) {
	movRXRYState2(cpu, clockIn, instruction, state2, controlBit)
}

func movRXatRYState2(cpu *CPU, clockIn []bool, instruction []bool, state2 []bool, controlBit []bool) {
	regOutput := readR(cpu, clockIn, instruction, true)

	andState2MOVrxAtry := AND(state2, controlBit)

	cpu.pc.SetInput("clockIn", clockIn)
	cpu.pc.SetInput("inc", andState2MOVrxAtry)
	cpu.pc.GetOutput("out")

	cpu.ram.SetInput("clockIn", clockIn)
	cpu.ram.SetInput("address", regOutput[:14])

	mux := MuxN.WithSize(cpu.busSize)
	mux.SetInput("a", cpu.bus.GetOutput("out"))
	mux.SetInput("b", append(cpu.ram.GetOutput("out"), make([]bool, cpu.byteSize)...))
	mux.SetInput("sel", andState2MOVrxAtry)
	cpu.bus.SetInput("in", mux.GetOutput("out"))
}

func movatRXatRYState2(cpu *CPU, clockIn []bool, instruction []bool, state2 []bool, controlBit []bool) {
	movRXatRYState2(cpu, clockIn, instruction, state2, controlBit)
}

func movRXValState2(cpu *CPU, clockIn []bool, instruction []bool, state2 []bool, controlBit []bool) {

	andState2MOVrxval := AND(state2, controlBit)

	cpu.pc.SetInput("clockIn", clockIn)
	cpu.pc.SetInput("inc", andState2MOVrxval)
	cpu.pc.GetOutput("out")

	mux := MuxN.WithSize(cpu.busSize)
	mux.SetInput("a", cpu.bus.GetOutput("out"))
	mux.SetInput("b", cpu.valueRegister.GetOutput("out"))
	mux.SetInput("sel", andState2MOVrxval)
	cpu.bus.SetInput("in", mux.GetOutput("out"))
}

func movRXRYState3(cpu *CPU, clockIn []bool, instruction []bool, state3 []bool, controlBit []bool) {
	writeR(cpu, clockIn, instruction, AND(state3, controlBit))
}

func movRXatRYState3(cpu *CPU, clockIn []bool, instruction []bool, state3 []bool, controlBit []bool) {
	movRXRYState3(cpu, clockIn, instruction, state3, controlBit)
}

func movatRXRYState3(cpu *CPU, clockIn []bool, instruction []bool, state3 []bool, controlBit []bool) {
	regOutput := readR(cpu, clockIn, instruction, false)

	cpu.ram.SetInput("clockIn", clockIn)
	cpu.ram.SetInput("address", regOutput[:14])
	cpu.ram.SetInput("load", AND(state3, controlBit))
	cpu.ram.SetInput("in", cpu.bus.GetOutput("out")[:8])
	cpu.ram.GetOutput("out")
}

func movatRXatRYState3(cpu *CPU, clockIn []bool, instruction []bool, state2 []bool, controlBit []bool) {
	movatRXRYState3(cpu, clockIn, instruction, state2, controlBit)
}

func movRXValState3(cpu *CPU, clockIn []bool, instruction []bool, state3 []bool, controlBit []bool) {
	movRXRYState3(cpu, clockIn, instruction, state3, controlBit)
}

func (cpu *CPU) step(clockIn []bool) {

	cpu.fsm.SetInput("clockIn", clockIn)

	dmux := DMux8Way.WithSize(1)
	dmux.SetInput("in", []bool{true})
	dmux.SetInput("sel", cpu.fsm.GetOutput("out")[:3])

	state0 := dmux.GetOutput("a")
	state1 := dmux.GetOutput("b")

	cpu.pc.SetInput("clockIn", clockIn)
	cpu.pc.SetInput("inc", OR(state1, cpu.value.GetOutput("out")))

	pcOut := cpu.pc.GetOutput("out")

	cpu.ram.SetInput("clockIn", clockIn)
	cpu.ram.SetInput("address", pcOut[:14])
	cpu.ram.SetInput("load", []bool{false})

	valueIRLoadDMux := DMuxN.WithSize(2)
	valueIRLoadDMux.SetInput("in", append(state0, state1...))
	valueIRLoadDMux.SetInput("sel", cpu.value.GetOutput("out"))

	currentByte := cpu.ram.GetOutput("out")

	FillTwoByteRegister(cpu.valueRegister, clockIn, currentByte, valueIRLoadDMux.GetOutput("b")[0], valueIRLoadDMux.GetOutput("b")[1])
	instruction := FillTwoByteRegister(cpu.insRegister, clockIn, currentByte, valueIRLoadDMux.GetOutput("a")[0], valueIRLoadDMux.GetOutput("a")[1])

	notValue := NOT(cpu.value.GetOutput("out"))
	cpu.value.SetInput("clockIn", clockIn)
	cpu.value.SetInput("load", state1)
	cpu.value.SetInput("in", AND(instruction[6:7], notValue))

	andState1Value := AND(state1, cpu.value.GetOutput("out"))
	cpu.fsm.SetInput("reset", andState1Value)
	state2 := dmux.GetOutput("c")
	state3 := dmux.GetOutput("d")

	cpu.fsm.SetInput("inc", NOT(andState1Value))
	cpu.fsm.GetOutput("out")
	cpu.fsm.SetInput("inc", []bool{false})

	decoder := DMux256Way.Clone()
	decoder.SetInput("in", OR(state2, state3))
	decoder.SetInput("sel", instruction[4:12])

	//state 2
	movRXRYState2(cpu, clockIn, instruction, state2, decoder.GetOutput("out")[:1])
	movRXatRYState2(cpu, clockIn, instruction, state2, decoder.GetOutput("out")[1:2])
	movatRXRYState2(cpu, clockIn, instruction, state2, decoder.GetOutput("out")[2:3])
	movatRXatRYState2(cpu, clockIn, instruction, state2, decoder.GetOutput("out")[3:4])
	movRXValState2(cpu, clockIn, instruction, state2, instruction[6:7])

	//state 3
	movRXRYState3(cpu, clockIn, instruction, state3, decoder.GetOutput("out")[:1])
	movRXatRYState3(cpu, clockIn, instruction, state3, decoder.GetOutput("out")[1:2])
	movatRXRYState3(cpu, clockIn, instruction, state3, decoder.GetOutput("out")[2:3])
	movatRXatRYState3(cpu, clockIn, instruction, state3, decoder.GetOutput("out")[3:4])
	movRXValState3(cpu, clockIn, instruction, state2, instruction[6:7])
}

func (cpu *CPU) Stop() {
	cpu.run = false
}

func (cpu *CPU) SetInput(in []bool) {
	cpu.bus.SetInput("in", in)
}

func (cpu *CPU) GetOutput() []bool {
	return cpu.bus.GetOutput("out")
}

func calculateControlSection(component *Component) {

}

func calculateFSM(component *Component) {
	clockIn := component.ports["clockIn"]
	inc := component.ports["inc"]
	reset := component.ports["reset"]

	pc := component.getComponent("pc", PC.WithSize(component.busSize))

	pc.SetInput("clockIn", clockIn)
	pc.SetInput("inc", inc)
	pcOut := pc.GetOutput("out")
	pc.SetInput("reset", OR(reset, []bool{pcOut[2]}))

	component.ports["out"] = pcOut
}

func calculateIR(component *Component) {
	in := component.ports["in"]
	load0 := component.ports["load0"]
	load1 := component.ports["load1"]
	clockIn := component.ports["clockIn"]

	register0 := component.getComponent("register0", Register.WithSize(component.busSize/2))
	register0.SetInput("in", in[:component.busSize/2])
	register0.SetInput("clockIn", clockIn)
	register0.SetInput("load", load0)

	register1 := component.getComponent("register1", Register.WithSize(component.busSize/2))
	register1.SetInput("in", in[component.busSize/2:])
	register1.SetInput("clockIn", clockIn)
	register1.SetInput("load", load1)

	component.ports["out"] = append(register0.GetOutput("out"), register1.GetOutput("out")...)
}
