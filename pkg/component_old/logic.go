package component_old

var (
	NANDGate = NewComponent(calculateNANDGate).
			WithInput("a").
			WithInput("b").
			WithOutput("out")

	NOTGate = NewComponent(calculateNOTGate).
		WithInput("in").
		WithOutput("out")

	NOTNGate = NewComponent(calculateNOTNGate).
			WithBusInput("in").
			WithBusOutput("out")

	ANDGate = NewComponent(calculateANDGate).
		WithInput("a").
		WithInput("b").
		WithOutput("out")

	ANDNGate = NewComponent(calculateANDNGate).
			WithBusInput("a").
			WithBusInput("b").
			WithBusOutput("out")

	ANDer = NewComponent(calculateANDer).
		WithBusInput("in").
		WithOutput("out")

	ORGate = NewComponent(calculateORGate).
		WithInput("a").
		WithInput("b").
		WithOutput("out")

	ORNGate = NewComponent(calculateORNGate).
		WithBusInput("a").
		WithBusInput("b").
		WithBusOutput("out")

	ORer = NewComponent(calculateORer).
		WithBusInput("in").
		WithOutput("out")

	ORNGate4Way = NewComponent(calculateORNGate4Way).
			WithBusInput("a").
			WithBusInput("b").
			WithBusInput("c").
			WithBusInput("d").
			WithBusOutput("out")

	XORGate = NewComponent(calculateXORGate).
		WithInput("a").
		WithInput("b").
		WithOutput("out")

	XORNGate = NewComponent(calculateXORNGate).
			WithBusInput("a").
			WithBusInput("b").
			WithBusOutput("out")

	ComparatorN = NewComponent(calculateComparatorN).
			WithBusInput("a").
			WithBusInput("b").
			WithBusOutput("out").
			WithOutput("aLarger").
			WithOutput("eq")
)

func calculateNANDGate(component *Component) {
	a := component.ports["a"][0]
	b := component.ports["b"][0]
	output := !(a && b)
	component.ports["out"] = []bool{output}
}

func calculateNOTGate(component *Component) {
	in := component.ports["in"]
	nand := NANDGate.Clone()
	nand.SetInput("a", in)
	nand.SetInput("b", in)
	component.ports["out"] = nand.GetOutput("out")
}

func calculateNOTNGate(component *Component) {
	in := component.ports["in"]

	out := []bool{}
	not := NOTGate.Clone()
	for i := uint(0); i < component.busSize; i++ {
		not.SetInput("in", []bool{in[i]})
		out = append(out, not.GetOutput("out")[0])
	}
	component.ports["out"] = out
}

func calculateANDGate(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]

	nand := NANDGate.Clone()
	nand.SetInput("a", a)
	nand.SetInput("b", b)

	not := NOTGate.Clone()
	not.SetInput("in", nand.GetOutput("out"))
	component.ports["out"] = not.GetOutput("out")
}

func calculateANDNGate(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]

	out := []bool{}
	and := ANDGate.Clone()
	for i := uint(0); i < component.busSize; i++ {
		and.SetInput("a", []bool{a[i]})
		and.SetInput("b", []bool{b[i]})
		out = append(out, and.GetOutput("out")[0])
	}
	component.ports["out"] = out
}

func calculateANDer(component *Component) {
	in := component.ports["in"]

	and := ANDGate.Clone()
	a := []bool{in[0]}
	for i := uint(1); i < component.busSize; i++ {
		and.SetInput("a", a)
		and.SetInput("b", []bool{in[i]})
		a = and.GetOutput("out")
	}
	component.ports["out"] = a
}

func calculateORGate(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]

	not1 := NOTGate.Clone()
	not1.SetInput("in", a)

	not2 := NOTGate.Clone()
	not2.SetInput("in", b)

	nand := NANDGate.Clone()
	nand.SetInput("a", not1.GetOutput("out"))
	nand.SetInput("b", not2.GetOutput("out"))
	component.ports["out"] = nand.GetOutput("out")
}

func calculateORNGate(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]

	out := []bool{}
	or := ORGate.Clone()
	for i := uint(0); i < component.busSize; i++ {
		or.SetInput("a", []bool{a[i]})
		or.SetInput("b", []bool{b[i]})
		out = append(out, or.GetOutput("out")[0])
	}
	component.ports["out"] = out
}

func calculateORer(component *Component) {
	in := component.ports["in"]

	or := ORGate.Clone()
	a := []bool{in[0]}
	for i := uint(1); i < component.busSize; i++ {
		or.SetInput("a", a)
		or.SetInput("b", []bool{in[i]})
		a = or.GetOutput("out")
	}
	component.ports["out"] = a
}

func calculateORNGate4Way(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]
	c := component.ports["c"]
	d := component.ports["d"]

	in := append(a, b...)
	in = append(in, c...)
	in = append(in, d...)
	or := ORer.WithSize(component.busSize * 4)
	or.SetInput("in", in)
	component.ports["out"] = or.GetOutput("out")
}

func calculateXORGate(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]

	notA := NOTGate.Clone()
	notA.SetInput("in", a)

	notB := NOTGate.Clone()
	notB.SetInput("in", b)

	and1 := ANDGate.Clone()
	and1.SetInput("a", a)
	and1.SetInput("b", notB.GetOutput("out"))

	and2 := ANDGate.Clone()
	and2.SetInput("a", notA.GetOutput("out"))
	and2.SetInput("b", b)

	or := ORGate.Clone()
	or.SetInput("a", and1.GetOutput("out"))
	or.SetInput("b", and2.GetOutput("out"))

	component.ports["out"] = or.GetOutput("out")
}

func calculateXORNGate(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]

	out := []bool{}
	xor := XORGate.Clone()
	for i := uint(0); i < component.busSize; i++ {
		xor.SetInput("a", []bool{a[i]})
		xor.SetInput("b", []bool{b[i]})
		out = append(out, xor.GetOutput("out")[0])
	}
	component.ports["out"] = out
}

func calculateComparatorN(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]

	out := []bool{}
	aLarger := []bool{false}
	eq := []bool{true}
	xor := XORGate.Clone()
	for i := int(component.busSize) - 1; i >= 0; i-- {
		xor.SetInput("a", []bool{a[i]})
		xor.SetInput("b", []bool{b[i]})

		uneq := xor.GetOutput("out")
		out = append(out, uneq[0])

		not := NOTGate.Clone()
		not.SetInput("in", uneq)

		and1 := ANDGate.Clone()
		and1.SetInput("a", not.GetOutput("out"))
		and1.SetInput("b", eq)

		and2 := ANDGate.Clone()
		and2.SetInput("a", eq)
		and2.SetInput("b", []bool{a[i]})

		and3 := ANDGate.Clone()
		and3.SetInput("a", and2.GetOutput("out"))
		and3.SetInput("b", uneq)

		or := ORGate.Clone()
		or.SetInput("a", and3.GetOutput("out"))
		or.SetInput("b", aLarger)

		eq = and1.GetOutput("out")
		aLarger = or.GetOutput("out")

	}
	component.ports["out"] = out
	component.ports["aLarger"] = aLarger
	component.ports["eq"] = eq
}
