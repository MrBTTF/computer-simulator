package component_old

import "testing"

func Test_HalfAdder(t *testing.T) {
	testSet := TestSet{
		inputsNames:  []string{"a", "b"},
		outputsNames: []string{"sum", "carry"},
		data: []TestEntry{
			{"00", "00"},
			{"10", "10"},
			{"01", "10"},
			{"11", "01"},
		},
	}
	c := HalfAdder.Clone()
	checkComponent(t, c, testSet)
}

func Test_FullAdder(t *testing.T) {
	testSet := TestSet{
		inputsNames:  []string{"a", "b", "c"},
		outputsNames: []string{"sum", "carry"},
		data: []TestEntry{
			{"000", "00"},
			{"001", "10"},
			{"010", "10"},
			{"011", "01"},

			{"100", "10"},
			{"101", "01"},
			{"110", "01"},
			{"111", "11"},
		},
	}
	c := FullAdder.Clone()
	checkComponent(t, c, testSet)
}

func Test_AdderN(t *testing.T) {
	testSet := TestSet{
		inputsNames:  []string{"a", "b", "carryIn"},
		outputsNames: []string{"out", "carryOut"},
		data: []TestEntry{
			{"00 00 0", "00 0"},
			{"10 01 0", "11 0"},
			{"01 00 0", "01 0"},
			{"00 01 0", "01 0"},
			{"11 00 0", "11 0"},
			{"11 01 0", "00 1"},

			{"11 11 0", "10 1"},
			{"10 10 0", "00 1"},
			{"01 01 0", "10 0"},
			{"01 10 0", "11 0"},

			{"11 11 1", "11 1"},
			{"10 10 1", "01 1"},
			{"01 01 1", "11 0"},
			{"01 10 1", "00 1"},
		},
	}
	c := AdderN.WithSize(2)
	checkComponent(t, c, testSet)
}

func Test_IncN(t *testing.T) {
	testSet := TestSet{
		inputsNames:  []string{"in"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"00", "01"},
			{"01", "10"},
			{"10", "11"},
			{"11", "00"},
		},
	}
	c := IncN.WithSize(2)
	checkComponent(t, c, testSet)
}

func Test_LeftShiftN(t *testing.T) {
	testSet := TestSet{
		inputsNames:  []string{"in", "carryIn"},
		outputsNames: []string{"out", "carryOut"},
		data: []TestEntry{
			{"00 0", "00 0"},
			{"01 0", "10 0"},
			{"11 0", "10 1"},

			{"00 1", "01 0"},
			{"01 1", "11 0"},
			{"11 1", "11 1"},
		},
	}
	c := LeftShiftN.WithSize(2)
	checkComponent(t, c, testSet)
}

func Test_RightShiftN(t *testing.T) {
	testSet := TestSet{
		inputsNames:  []string{"in", "carryIn"},
		outputsNames: []string{"out", "carryOut"},
		data: []TestEntry{
			{"00 0", "00 0"},
			{"01 0", "00 1"},
			{"11 0", "01 1"},

			{"00 1", "10 0"},
			{"01 1", "10 1"},
			{"11 1", "11 1"},
		},
	}
	c := RightShiftN.WithSize(2)
	checkComponent(t, c, testSet)
}

func Test_ALU(t *testing.T) {
	testSet := TestSet{
		inputsNames:  []string{"a", "b", "carryIn", "op"},
		outputsNames: []string{"out", "carryOut", "aLarger", "eq", "zero"},
		data: []TestEntry{
			{"01 01 0 000", "10 0 0 1 0"},
			{"01 01 1 000", "11 0 0 1 0"},
			{"01 11 0 000", "00 1 0 0 1"},
			{"01 11 1 000", "01 1 0 0 0"},

			{"01 11 0 001", "00 1 0 0 1"},
			{"01 11 1 001", "10 1 0 0 0"},

			{"01 11 0 010", "10 0 0 0 0"},
			{"01 11 1 010", "11 0 0 0 0"},

			{"01 11 0 011", "10 0 0 0 0"},

			{"01 10 0 100", "00 0 0 0 1"},

			{"01 10 0 101", "11 0 0 0 0"},

			{"11 11 0 110", "00 0 0 1 1"},

			{"11 11 0 111", "00 0 0 1 1"},
			{"11 10 0 111", "00 0 1 0 1"},
			{"01 10 0 111", "00 0 0 0 1"},
		},
	}
	c := ALU.WithSize(2)
	checkComponent(t, c, testSet)
}
