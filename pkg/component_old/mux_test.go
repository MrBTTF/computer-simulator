package component_old

import "testing"

func Test_Mux(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b", "sel"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"000", "0"},
			{"100", "1"},
			{"010", "0"},
			{"110", "1"},
			{"001", "0"},
			{"101", "0"},
			{"011", "1"},
			{"111", "1"},
		},
	}
	c := Mux.Clone()
	checkComponent(t, c, testSet)
}

func Test_MuxN(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b", "sel"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"1000 0", "10"},
			{"0011 1", "11"},
			{"0110 0", "01"},
			{"0110 1", "10"},
		},
	}
	c := MuxN.WithSize(2)
	checkComponent(t, c, testSet)
}

func Test_Mux4Way(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b", "c", "d", "sel"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"1000 00", "1"},
			{"0100 01", "1"},
			{"0010 10", "1"},
			{"0001 11", "1"},
		},
	}
	c := Mux4Way.Clone()
	checkComponent(t, c, testSet)
}

func Test_Mux8Way(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b", "c", "d", "e", "f", "g", "h", "sel"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"1000 0000 000", "1"},
			{"0100 0000 001", "1"},
			{"0010 0000 010", "1"},
			{"0001 0000 011", "1"},

			{"0000 1000 100", "1"},
			{"0000 0100 101", "1"},
			{"0000 0010 110", "1"},
			{"0000 0001 111", "1"},
		},
	}
	c := Mux8Way.Clone()
	checkComponent(t, c, testSet)
}

func Test_DMux(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in", "sel"},
		outputsNames: []string{"a", "b"},
		data: []TestEntry{
			{"00", "00"},
			{"10", "10"},
			{"01", "00"},
			{"11", "01"},
		},
	}
	c := DMux.Clone()
	checkComponent(t, c, testSet)
}

func Test_DMuxN(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in", "sel"},
		outputsNames: []string{"a", "b"},
		data: []TestEntry{
			{"000", "0000"},
			{"100", "1000"},
			{"010", "0100"},
			{"110", "1100"},

			{"001", "0000"},
			{"101", "0010"},
			{"011", "0001"},
			{"111", "0011"},
		},
	}
	c := DMuxN.WithSize(2)
	checkComponent(t, c, testSet)
}

func Test_DMux4Way(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in", "sel"},
		outputsNames: []string{"a", "b", "c", "d"},
		data: []TestEntry{
			{"1 00", "1000"},
			{"1 01", "0100"},
			{"1 10", "0010"},
			{"1 11", "0001"},
		},
	}
	c := DMux4Way.Clone()
	checkComponent(t, c, testSet)
}

func Test_DMux8Way(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in", "sel"},
		outputsNames: []string{"a", "b", "c", "d", "e", "f", "g", "h"},
		data: []TestEntry{
			{"1 000", "1000 0000"},
			{"1 001", "0100 0000"},
			{"1 010", "0010 0000"},
			{"1 011", "0001 0000"},

			{"1 100", "0000 1000"},
			{"1 101", "0000 0100"},
			{"1 110", "0000 0010"},
			{"1 111", "0000 0001"},
		},
	}
	c := DMux8Way.Clone()
	checkComponent(t, c, testSet)
}

func Test_DMux256Way(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in", "sel"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"100", "0001"},
			{"101", "0010"},
			{"110", "0100"},
			{"111", "1000"},
		},
	}
	c := DMux256Way.
		WithInputWithSize("sel", 2).
		WithOutputWithSize("out", 4).Clone()
	checkComponent(t, c, testSet)
}
