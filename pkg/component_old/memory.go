package component_old

import (
	"computer-simulator/pkg/utils"
	"fmt"
	"strconv"
)

var (
	DFF = NewComponent(calculateDFF).
		WithInput("in").
		WithInput("clockIn").
		WithOutput("out")

	Bit = NewComponent(calculateBit).
		WithInput("in").
		WithInput("load").
		WithInput("clockIn").
		WithOutput("out")

	Register = NewComponent(calculateRegister).
			WithBusInput("in").
			WithInput("load").
			WithInput("clockIn").
			WithBusOutput("out")

	RAM16K = NewComponent(calculateRAM16K).
		WithBusInput("in").
		WithInput("load").
		WithInputWithSize("address", 14).
		WithInput("clockIn").
		WithBusOutput("out")
)

func calculateDFF(component *Component) {
	in := component.ports["in"]
	clockIn := component.ports["clockIn"]

	not := NOTGate.Clone()
	not.SetInput("in", in)
	notIn := not.GetOutput("out")

	nand1 := NANDGate.Clone()
	nand1.SetInput("a", in)
	nand1.SetInput("b", clockIn)

	nand2 := NANDGate.Clone()
	nand2.SetInput("a", notIn)
	nand2.SetInput("b", clockIn)

	nand4 := component.getComponent("nand4", NANDGate)
	nand4.SetInput("a", nand2.GetOutput("out"))

	nand3 := NANDGate.Clone()
	nand3.SetInput("a", nand4.GetOutput("out"))
	nand3.SetInput("b", nand1.GetOutput("out"))

	nand4.SetInput("b", nand3.GetOutput("out"))

	component.ports["out"] = nand3.GetOutput("out")
}

func calculateBit(component *Component) {
	in := component.ports["in"]
	load := component.ports["load"]
	clockIn := component.ports["clockIn"]

	dff := component.getComponent("dff", DFF)
	mux := Mux.Clone()
	mux.SetInput("a", dff.GetOutput("out"))
	mux.SetInput("b", in)
	mux.SetInput("sel", load)

	dff.SetInput("in", mux.GetOutput("out"))
	dff.SetInput("clockIn", clockIn)

	component.ports["out"] = dff.GetOutput("out")
}

func calculateRegister(component *Component) {
	in := component.ports["in"]
	load := component.ports["load"]
	clockIn := component.ports["clockIn"]

	out := []bool{}
	for i := uint(0); i < component.busSize; i++ {
		name := fmt.Sprintf("bit%d", i)
		bit := component.getComponent(name, Bit)
		bit.SetInput("in", []bool{in[i]})
		bit.SetInput("load", load)
		bit.SetInput("clockIn", clockIn)
		out = append(out, bit.GetOutput("out")[0])
	}

	component.ports["out"] = out
}

func calculateRAM16K(component *Component) {
	in := component.ports["in"]
	load := component.ports["load"]
	address := component.ports["address"]
	clockIn := component.ports["clockIn"]

	i, err := strconv.ParseInt(utils.BinToString(address), 2, 15)
	if err != nil {
		panic(err)
	}
	name := fmt.Sprintf("register%d", i)
	register := component.getComponent(name, Register.WithSize(component.busSize))
	register.SetInput("in", in)
	register.SetInput("clockIn", clockIn)
	register.SetInput("load", load)

	component.ports["out"] = register.GetOutput("out")
}
