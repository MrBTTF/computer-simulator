package component_old

import (
	"fmt"
)

// IComponent is an abstract object which has N ports and N ports
type IComponent interface {
	SetInput(input string, value []bool)
	GetOutput(output string) []bool
	WithSize(busSize uint) IComponent
	Clone() IComponent
}

//Component factory
type Component struct {
	inputs  map[string]uint
	outputs map[string]uint

	busInputs  []string
	busOutputs []string

	ports      map[string][]bool
	components map[string]IComponent

	busSize uint

	Calculate func(component *Component)
}

func NewComponent(calculateFunc func(component *Component)) *Component {
	return &Component{
		Calculate: calculateFunc,
		inputs:    make(map[string]uint),
		outputs:   make(map[string]uint),
		busSize:   1,
	}
}

func (c *Component) getComponent(name string, defaultComponent IComponent) IComponent {
	component, exists := c.components[name]
	if !exists {
		c.components[name] = defaultComponent.Clone()
		return c.components[name]
	}
	return component
}

func (c *Component) WithInputWithSize(input string, size uint) *Component {
	c.inputs[input] = size
	return c
}

func (c *Component) WithInput(input string) *Component {
	return c.WithInputWithSize(input, 1)
}

func (c *Component) WithBusInput(input string) *Component {
	c.WithInput(input)
	c.busInputs = append(c.busInputs, input)
	return c
}

func (c *Component) WithOutputWithSize(output string, size uint) *Component {
	c.outputs[output] = size
	return c
}

func (c *Component) WithOutput(output string) *Component {
	return c.WithOutputWithSize(output, 1)
}

func (c *Component) WithBusOutput(output string) *Component {
	c.WithOutput(output)
	c.busOutputs = append(c.busOutputs, output)
	return c
}

func (c *Component) SetInput(input string, value []bool) {
	size, exists := c.inputs[input]
	if !exists {
		panic(fmt.Sprintf("component does not have '%s' input", input))
	}
	if len(value) != int(size) {
		panic(fmt.Sprintf("input must have size %d", size))
	}
	c.ports[input] = value
}

func (c *Component) GetOutput(output string) []bool {
	_, exists := c.outputs[output]
	if !exists {
		panic(fmt.Sprintf("component does not have '%s' output", output))
	}
	c.Calculate(c)
	return c.ports[output]
}

func (c *Component) WithSize(busSize uint) IComponent {
	component := new(Component)
	component.inputs = make(map[string]uint)
	component.outputs = make(map[string]uint)
	component.busInputs = c.busInputs
	component.busOutputs = c.busOutputs
	component.ports = make(map[string][]bool)
	component.components = make(map[string]IComponent)
	component.busSize = busSize
	component.Calculate = c.Calculate

	for input, size := range c.inputs {
		component.inputs[input] = size
	}

	for output, size := range c.outputs {
		component.outputs[output] = size
	}

	for _, input := range c.busInputs {
		component.inputs[input] = component.busSize
	}
	for _, output := range c.busOutputs {
		component.outputs[output] = component.busSize
	}
	for input, size := range component.inputs {
		component.ports[input] = make([]bool, size)
	}
	return component
}

func (c *Component) Clone() IComponent {
	return c.WithSize(c.busSize)
}

var (
	Enabler = NewComponent(calculateEnabler).
		WithBusInput("in").
		WithInput("e").
		WithBusOutput("out")

	PC = NewComponent(calculatePC).
		WithBusInput("in").
		WithInput("load").
		WithInput("inc").
		WithInput("reset").
		WithInput("clockIn").
		WithBusOutput("out")

	Bus = NewComponent(calculateBus).
		WithBusInput("in").
		WithBusOutput("out")
)

func calculateEnabler(component *Component) {
	in := component.ports["in"]
	e := component.ports["e"]

	var out []bool
	and := ANDGate.Clone()
	for _, value := range in {
		and.SetInput("a", e)
		and.SetInput("b", []bool{value})
		out = append(out, and.GetOutput("out")[0])
	}
	component.ports["out"] = out
}

func calculateBus(component *Component) {
	component.ports["out"] = component.ports["in"]
}

func calculatePC(component *Component) {
	in := component.ports["in"]
	load := component.ports["load"]
	inc := component.ports["inc"]
	reset := component.ports["reset"]
	clockIn := component.ports["clockIn"]

	register := component.getComponent("register", Register.WithSize(component.busSize))

	incN := IncN.WithSize(component.busSize)
	incN.SetInput("in", register.GetOutput("out"))

	mux1 := MuxN.WithSize(component.busSize)
	mux1.SetInput("a", register.GetOutput("out"))
	mux1.SetInput("b", incN.GetOutput("out"))
	mux1.SetInput("sel", inc)

	mux2 := MuxN.WithSize(component.busSize)
	mux2.SetInput("a", mux1.GetOutput("out"))
	mux2.SetInput("b", in)
	mux2.SetInput("sel", load)

	mux3 := MuxN.WithSize(component.busSize)
	mux3.SetInput("a", mux2.GetOutput("out"))
	mux3.SetInput("b", make([]bool, component.busSize))
	mux3.SetInput("sel", reset)

	register.SetInput("in", mux3.GetOutput("out"))
	register.SetInput("clockIn", clockIn)
	register.SetInput("load", []bool{true})

	component.ports["out"] = register.GetOutput("out")
}
