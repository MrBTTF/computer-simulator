package component_old

import "testing"

func Test_NAND(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"00", "1"},
			{"10", "1"},
			{"01", "1"},
			{"11", "0"},
		},
	}
	c := NANDGate.Clone()
	checkComponent(t, c, testSet)
}

func Test_NOT(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"0", "1"},
			{"1", "0"},
		},
	}
	c := NOTGate.Clone()
	checkComponent(t, c, testSet)
}

func Test_AND(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"00", "0"},
			{"10", "0"},
			{"01", "0"},
			{"11", "1"},
		},
	}
	c := ANDGate.Clone()
	checkComponent(t, c, testSet)
}

func Test_ANDN(t *testing.T) {
	testSet := TestSet{
		inputsNames:  []string{"a", "b"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"00 11", "00"},
			{"10 01", "00"},
			{"01 01", "01"},
			{"11 11", "11"},
		},
	}
	c := ANDNGate.WithSize(2)
	checkComponent(t, c, testSet)
}

func Test_ANDerN(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"000", "0"},
			{"001", "0"},
			{"010", "0"},
			{"011", "0"},

			{"100", "0"},
			{"101", "0"},
			{"110", "0"},
			{"111", "1"},
		},
	}
	c := ANDer.WithSize(3)
	checkComponent(t, c, testSet)
}

func Test_OR(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"00", "0"},
			{"10", "1"},
			{"01", "1"},
			{"11", "1"},
		},
	}
	c := ORGate.Clone()
	checkComponent(t, c, testSet)
}

func Test_ORN(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"00 11", "11"},
			{"10 01", "11"},
			{"01 01", "01"},
			{"11 11", "11"},
			{"00 00", "00"},
		},
	}
	c := ORNGate.WithSize(2)
	checkComponent(t, c, testSet)
}

func Test_ORerN(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"000", "0"},
			{"001", "1"},
			{"010", "1"},
			{"011", "1"},

			{"100", "1"},
			{"101", "1"},
			{"110", "1"},
			{"111", "1"},
		},
	}
	c := ORer.WithSize(3)
	checkComponent(t, c, testSet)
}

func Test_ORNGate4Way(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b", "c", "d"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"0000", "0"},
			{"0001", "1"},
			{"0010", "1"},
			{"0011", "1"},

			{"0100", "1"},
			{"0101", "1"},
			{"0110", "1"},
			{"0111", "1"},

			{"1000", "1"},
			{"1001", "1"},
			{"1010", "1"},
			{"1011", "1"},

			{"1100", "1"},
			{"1101", "1"},
			{"1110", "1"},
			{"1111", "1"},
		},
	}
	c := ORNGate4Way.Clone()
	checkComponent(t, c, testSet)
}

func Test_XOR(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"00", "0"},
			{"10", "1"},
			{"01", "1"},
			{"11", "0"},
		},
	}
	c := XORGate.Clone()
	checkComponent(t, c, testSet)
}

func Test_XORN(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"00 11", "11"},
			{"10 01", "11"},
			{"01 01", "00"},
			{"11 11", "00"},
			{"00 00", "00"},
		},
	}
	c := XORNGate.WithSize(2)
	checkComponent(t, c, testSet)
}

func Test_ComparatorN(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"a", "b"},
		outputsNames: []string{"aLarger", "eq"},
		data: []TestEntry{
			{"00 11", "00"},
			{"10 01", "10"},
			{"01 10", "00"},
			{"11 00", "10"},

			{"00 00", "01"},
			{"11 11", "01"},
			{"01 01", "01"},
		},
	}
	c := ComparatorN.WithSize(2)
	checkComponent(t, c, testSet)
}
