package component_old

var (
	HalfAdder = NewComponent(calculateHalfAdder).
			WithInput("a").
			WithInput("b").
			WithOutput("sum").
			WithOutput("carry")

	FullAdder = NewComponent(calculateFullAdder).
			WithInput("a").
			WithInput("b").
			WithInput("c").
			WithOutput("sum").
			WithOutput("carry")

	AdderN = NewComponent(calculateAdderN).
		WithBusInput("a").
		WithBusInput("b").
		WithInput("carryIn").
		WithBusOutput("out").
		WithOutput("carryOut")

	IncN = NewComponent(calculateIncN).
		WithBusInput("in").
		WithBusOutput("out")

	LeftShiftN = NewComponent(calculateLeftShiftN).
			WithBusInput("in").
			WithInput("carryIn").
			WithBusOutput("out").
			WithOutput("carryOut")

	RightShiftN = NewComponent(calculateRightShiftN).
			WithBusInput("in").
			WithInput("carryIn").
			WithBusOutput("out").
			WithOutput("carryOut")

	ALU = NewComponent(calculateALU).
		WithBusInput("a").
		WithBusInput("b").
		WithInput("carryIn").
		WithInputWithSize("op", 3).
		WithBusOutput("out").
		WithOutput("carryOut").
		WithOutput("aLarger").
		WithOutput("eq").
		WithOutput("zero")
)

func calculateHalfAdder(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]

	xor := XORGate.Clone()
	xor.SetInput("a", a)
	xor.SetInput("b", b)

	and := ANDGate.Clone()
	and.SetInput("a", a)
	and.SetInput("b", b)

	component.ports["sum"] = xor.GetOutput("out")
	component.ports["carry"] = and.GetOutput("out")
}

func calculateFullAdder(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]
	c := component.ports["c"]

	halfAdder1 := HalfAdder.Clone()
	halfAdder1.SetInput("a", a)
	halfAdder1.SetInput("b", b)

	halfAdder2 := HalfAdder.Clone()
	halfAdder2.SetInput("a", halfAdder1.GetOutput("sum"))
	halfAdder2.SetInput("b", c)

	or := XORGate.Clone()
	or.SetInput("a", halfAdder1.GetOutput("carry"))
	or.SetInput("b", halfAdder2.GetOutput("carry"))

	component.ports["sum"] = halfAdder2.GetOutput("sum")
	component.ports["carry"] = or.GetOutput("out")
}

func calculateAdderN(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]
	carry := component.ports["carryIn"]

	out := []bool{}
	fullAdder := FullAdder.Clone()
	for i := uint(0); i < component.busSize; i++ {
		fullAdder.SetInput("a", []bool{a[i]})
		fullAdder.SetInput("b", []bool{b[i]})
		fullAdder.SetInput("c", carry)
		out = append(out, fullAdder.GetOutput("sum")[0])

		carry = fullAdder.GetOutput("carry")
	}
	component.ports["out"] = out
	component.ports["carryOut"] = carry
}

func calculateIncN(component *Component) {
	in := component.ports["in"]

	one := make([]bool, component.busSize)
	one[0] = true

	adderN := AdderN.WithSize(component.busSize)
	adderN.SetInput("a", in)
	adderN.SetInput("b", one)

	component.ports["out"] = adderN.GetOutput("out")
}

func calculateLeftShiftN(component *Component) {
	in := component.ports["in"]
	carryIn := component.ports["carryIn"]

	component.ports["out"] = append(carryIn, in[:len(in)-1]...)
	component.ports["carryOut"] = in[len(in)-1:]
}

func calculateRightShiftN(component *Component) {
	in := component.ports["in"]
	carryIn := component.ports["carryIn"]

	component.ports["out"] = append(in[1:], carryIn[0])
	component.ports["carryOut"] = []bool{in[0]}
}

func calculateALU(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]
	carryIn := component.ports["carryIn"]
	op := component.ports["op"]

	add := AdderN.WithSize(component.busSize)
	add.SetInput("a", a)
	add.SetInput("b", b)
	add.SetInput("carryIn", carryIn)

	rsh := RightShiftN.WithSize(component.busSize)
	rsh.SetInput("in", a)
	rsh.SetInput("carryIn", carryIn)

	lsh := LeftShiftN.WithSize(component.busSize)
	lsh.SetInput("in", a)
	lsh.SetInput("carryIn", carryIn)

	not := NOTNGate.WithSize(component.busSize)
	not.SetInput("in", a)

	and := ANDNGate.WithSize(component.busSize)
	and.SetInput("a", a)
	and.SetInput("b", b)

	or := ORNGate.WithSize(component.busSize)
	or.SetInput("a", a)
	or.SetInput("b", b)

	comp := ComparatorN.WithSize(component.busSize)
	comp.SetInput("a", a)
	comp.SetInput("b", b)

	mux := Mux8Way.WithSize(component.busSize)
	mux.SetInput("a", add.GetOutput("out"))
	mux.SetInput("b", rsh.GetOutput("out"))
	mux.SetInput("c", lsh.GetOutput("out"))
	mux.SetInput("d", not.GetOutput("out"))
	mux.SetInput("e", and.GetOutput("out"))
	mux.SetInput("f", or.GetOutput("out"))
	mux.SetInput("g", comp.GetOutput("out"))
	mux.SetInput("sel", op)

	muxCarry := Mux4Way.Clone()
	muxCarry.SetInput("a", add.GetOutput("carryOut"))
	muxCarry.SetInput("b", rsh.GetOutput("carryOut"))
	muxCarry.SetInput("c", lsh.GetOutput("carryOut"))
	muxCarry.SetInput("d", []bool{false})
	muxCarry.SetInput("sel", op[:2])

	muxCarry2 := Mux.Clone()
	muxCarry2.SetInput("a", muxCarry.GetOutput("out"))
	muxCarry2.SetInput("b", []bool{false})
	muxCarry2.SetInput("sel", []bool{op[2]})

	orer := ORer.WithSize(component.busSize)
	orer.SetInput("in", mux.GetOutput("out"))
	notZero := NOTGate.Clone()
	notZero.SetInput("in", orer.GetOutput("out"))

	component.ports["out"] = mux.GetOutput("out")

	component.ports["carryOut"] = muxCarry2.GetOutput("out")
	component.ports["aLarger"] = comp.GetOutput("aLarger")
	component.ports["eq"] = comp.GetOutput("eq")
	component.ports["zero"] = notZero.GetOutput("out")

}
