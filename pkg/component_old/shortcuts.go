package component_old

func AND(a, b []bool) []bool {
	c := ANDGate.Clone()
	c.SetInput("a", a)
	c.SetInput("b", b)
	return c.GetOutput("out")
}

func OR(a, b []bool) []bool {
	c := ORGate.Clone()
	c.SetInput("a", a)
	c.SetInput("b", b)
	return c.GetOutput("out")
}

func NOT(in []bool) []bool {
	c := NOTGate.Clone()
	c.SetInput("in", in)
	return c.GetOutput("out")
}

func FillTwoByteRegister(valueRegister IComponent, clockIn, currentByte []bool, load0, load1 bool) []bool {
	valueRegister.SetInput("clockIn", clockIn)
	valueRegister.SetInput("load0", []bool{load0})
	valueRegister.SetInput("load1", []bool{load1})
	valueRegister.SetInput("in", append(currentByte, currentByte...))
	return valueRegister.GetOutput("out")
}
