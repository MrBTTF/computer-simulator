package component_old

import (
	"computer-simulator/pkg/utils"
	"fmt"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

type TestEntry struct {
	inputs  string
	outputs string
}

type TestSet struct {
	inputsNames  []string
	outputsNames []string
	data         []TestEntry
	component    *Component
}

func (ts *TestSet) GetEntry(i int) (inputs, outputs map[string]string, err error) {
	data := ts.data[i]
	data.inputs = strings.Replace(data.inputs, " ", "", -1)
	data.outputs = strings.Replace(data.outputs, " ", "", -1)

	inputs = map[string]string{}
	start := 0
	for _, input := range ts.inputsNames {
		size := int(ts.component.inputs[input])
		inputs[input] = data.inputs[start : start+size]
		start += size
	}
	if len(data.inputs) > start {
		err = fmt.Errorf("input value is too big: %s", data.inputs)
		return
	}

	outputs = map[string]string{}
	start = 0
	for _, output := range ts.outputsNames {
		size := int(ts.component.outputs[output])
		outputs[output] = data.outputs[start : start+size]
		start += size
	}
	if len(data.outputs) > start {
		err = fmt.Errorf("output value is too big: %s", data.outputs)
		return
	}
	return
}

func checkComponent(t *testing.T, component IComponent, testSet TestSet) {
	testSet.component = component.(*Component)
	for i := 0; i < len(testSet.data); i++ {
		inputs, outputs, err := testSet.GetEntry(i)
		assert.NoError(t, err)
		for input, value := range inputs {
			bin := utils.StringToBin(value, len(value))
			component.SetInput(input, bin)
		}
		for output, value := range outputs {
			strOutput := utils.BinToString(component.GetOutput(output))
			assert.Equal(t, value, strOutput, "output: %s, input: %v", output, inputs)
		}
	}
}

func Test_Enabler(t *testing.T) {
	testSet := TestSet{
		inputsNames:  []string{"in", "e"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"000", "00"},
			{"010", "00"},
			{"100", "00"},
			{"110", "00"},
			{"001", "00"},
			{"011", "01"},
			{"101", "10"},
			{"111", "11"},
		},
	}

	c := Enabler.WithSize(2)
	checkComponent(t, c, testSet)
}

func Test_PC(t *testing.T) {
	testSet := TestSet{
		inputsNames:  []string{"in", "load", "inc", "reset", "clockIn"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"00 0 0 0 1", "00"},
			{"00 0 0 0 0", "00"},
			{"00 0 0 0 1", "00"},
			{"00 0 0 0 0", "00"},

			{"00 0 1 0 1", "01"},
			{"00 0 1 0 0", "01"},
			{"00 0 0 0 1", "01"},
			{"00 0 0 0 0", "01"},

			{"00 0 1 0 1", "10"},
			{"00 0 1 0 0", "10"},
			{"00 0 0 0 1", "10"},
			{"00 0 0 0 0", "10"},
			{"00 0 0 0 1", "10"},
			{"00 0 0 0 0", "10"},

			{"00 0 1 0 1", "11"},
			{"00 0 1 0 0", "11"},

			{"01 1 0 0 1", "01"},
			{"01 1 0 0 0", "01"},

			{"01 0 0 1 1", "00"},
			{"01 0 0 1 0", "00"},
		},
	}

	c := PC.WithSize(2)
	checkComponent(t, c, testSet)
}
