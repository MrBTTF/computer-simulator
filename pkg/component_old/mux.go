package component_old

import (
	"strconv"

	"computer-simulator/pkg/utils"
)

var (
	Mux = NewComponent(calculateMux).
		WithInput("a").
		WithInput("b").
		WithInput("sel").
		WithOutput("out")

	MuxN = NewComponent(calculateMuxN).
		WithBusInput("a").
		WithBusInput("b").
		WithInput("sel").
		WithBusOutput("out")

	Mux4Way = NewComponent(calculateMux4Way).
		WithBusInput("a").
		WithBusInput("b").
		WithBusInput("c").
		WithBusInput("d").
		WithInputWithSize("sel", 2).
		WithBusOutput("out")

	Mux8Way = NewComponent(calculateMux8Way).
		WithBusInput("a").
		WithBusInput("b").
		WithBusInput("c").
		WithBusInput("d").
		WithBusInput("e").
		WithBusInput("f").
		WithBusInput("g").
		WithBusInput("h").
		WithInputWithSize("sel", 3).
		WithBusOutput("out")

	DMux = NewComponent(calculateDMux).
		WithInput("in").
		WithInput("sel").
		WithOutput("a").
		WithOutput("b")

	DMuxN = NewComponent(calculateDMuxN).
		WithBusInput("in").
		WithInput("sel").
		WithBusOutput("a").
		WithBusOutput("b")

	DMux4Way = NewComponent(calculateDMux4Way).
			WithBusInput("in").
			WithInputWithSize("sel", 2).
			WithBusOutput("a").
			WithBusOutput("b").
			WithBusOutput("c").
			WithBusOutput("d")

	DMux8Way = NewComponent(calculateDMux8Way).
			WithBusInput("in").
			WithInputWithSize("sel", 3).
			WithBusOutput("a").
			WithBusOutput("b").
			WithBusOutput("c").
			WithBusOutput("d").
			WithBusOutput("e").
			WithBusOutput("f").
			WithBusOutput("g").
			WithBusOutput("h")

	DMux256Way = NewComponent(calculateDMux256Way).
			WithInput("in").
			WithInputWithSize("sel", 8).
			WithOutputWithSize("out", 256)
)

func calculateMux(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]
	sel := component.ports["sel"]

	not := NOTGate.Clone()
	not.SetInput("in", sel)

	and1 := ANDGate.Clone()
	and1.SetInput("a", a)
	and1.SetInput("b", not.GetOutput("out"))

	and2 := ANDGate.Clone()
	and2.SetInput("a", b)
	and2.SetInput("b", sel)

	or := ORGate.Clone()
	or.SetInput("a", and1.GetOutput("out"))
	or.SetInput("b", and2.GetOutput("out"))

	component.ports["out"] = or.GetOutput("out")
}

func calculateMuxN(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]
	sel := component.ports["sel"]

	out := []bool{}
	mux := Mux.Clone()
	for i := uint(0); i < component.busSize; i++ {
		mux.SetInput("a", []bool{a[i]})
		mux.SetInput("b", []bool{b[i]})
		mux.SetInput("sel", sel)
		out = append(out, mux.GetOutput("out")[0])
	}
	component.ports["out"] = out
}

func calculateMux4Way(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]
	c := component.ports["c"]
	d := component.ports["d"]
	sel := component.ports["sel"]

	mux1 := MuxN.WithSize(component.busSize)
	mux1.SetInput("a", a)
	mux1.SetInput("b", b)
	mux1.SetInput("sel", []bool{sel[0]})

	mux2 := MuxN.WithSize(component.busSize)
	mux2.SetInput("a", c)
	mux2.SetInput("b", d)
	mux2.SetInput("sel", []bool{sel[0]})

	mux3 := MuxN.WithSize(component.busSize)
	mux3.SetInput("a", mux1.GetOutput("out"))
	mux3.SetInput("b", mux2.GetOutput("out"))
	mux3.SetInput("sel", []bool{sel[1]})

	component.ports["out"] = mux3.GetOutput("out")
}

func calculateMux8Way(component *Component) {
	a := component.ports["a"]
	b := component.ports["b"]
	c := component.ports["c"]
	d := component.ports["d"]
	e := component.ports["e"]
	f := component.ports["f"]
	g := component.ports["g"]
	h := component.ports["h"]
	sel := component.ports["sel"]

	mux1 := Mux4Way.WithSize(component.busSize)
	mux1.SetInput("a", a)
	mux1.SetInput("b", b)
	mux1.SetInput("c", c)
	mux1.SetInput("d", d)
	mux1.SetInput("sel", sel[:2])

	mux2 := Mux4Way.WithSize(component.busSize)
	mux2.SetInput("a", e)
	mux2.SetInput("b", f)
	mux2.SetInput("c", g)
	mux2.SetInput("d", h)
	mux2.SetInput("sel", sel[:2])

	mux3 := MuxN.WithSize(component.busSize)
	mux3.SetInput("a", mux1.GetOutput("out"))
	mux3.SetInput("b", mux2.GetOutput("out"))
	mux3.SetInput("sel", []bool{sel[2]})

	component.ports["out"] = mux3.GetOutput("out")
}

func calculateDMux(component *Component) {
	in := component.ports["in"]
	sel := component.ports["sel"]

	not := NOTGate.Clone()
	not.SetInput("in", sel)

	and1 := ANDGate.Clone()
	and1.SetInput("a", in)
	and1.SetInput("b", not.GetOutput("out"))

	and2 := ANDGate.Clone()
	and2.SetInput("a", in)
	and2.SetInput("b", sel)

	component.ports["a"] = and1.GetOutput("out")
	component.ports["b"] = and2.GetOutput("out")
}

func calculateDMuxN(component *Component) {
	in := component.ports["in"]
	sel := component.ports["sel"]

	a := []bool{}
	b := []bool{}
	dmux := DMux.Clone()
	for i := uint(0); i < component.busSize; i++ {
		dmux.SetInput("in", []bool{in[i]})
		dmux.SetInput("sel", sel)
		a = append(a, dmux.GetOutput("a")[0])
		b = append(b, dmux.GetOutput("b")[0])
	}
	component.ports["a"] = a
	component.ports["b"] = b
}

func calculateDMux4Way(component *Component) {
	in := component.ports["in"]
	sel := component.ports["sel"]

	dmux1 := DMuxN.WithSize(component.busSize)
	dmux1.SetInput("in", in)
	dmux1.SetInput("sel", []bool{sel[1]})

	dmux2 := DMuxN.WithSize(component.busSize)
	dmux2.SetInput("in", dmux1.GetOutput("a"))
	dmux2.SetInput("sel", []bool{sel[0]})

	dmux3 := DMuxN.WithSize(component.busSize)
	dmux3.SetInput("in", dmux1.GetOutput("b"))
	dmux3.SetInput("sel", []bool{sel[0]})

	component.ports["a"] = dmux2.GetOutput("a")
	component.ports["b"] = dmux2.GetOutput("b")
	component.ports["c"] = dmux3.GetOutput("a")
	component.ports["d"] = dmux3.GetOutput("b")
}

func calculateDMux8Way(component *Component) {
	in := component.ports["in"]
	sel := component.ports["sel"]

	dmux1 := DMuxN.WithSize(component.busSize)
	dmux1.SetInput("in", in)
	dmux1.SetInput("sel", []bool{sel[2]})

	dmux2 := DMux4Way.WithSize(component.busSize)
	dmux2.SetInput("in", dmux1.GetOutput("a"))
	dmux2.SetInput("sel", sel[:2])

	dmux3 := DMux4Way.WithSize(component.busSize)
	dmux3.SetInput("in", dmux1.GetOutput("b"))
	dmux3.SetInput("sel", sel[:2])

	component.ports["a"] = dmux2.GetOutput("a")
	component.ports["b"] = dmux2.GetOutput("b")
	component.ports["c"] = dmux2.GetOutput("c")
	component.ports["d"] = dmux2.GetOutput("d")

	component.ports["e"] = dmux3.GetOutput("a")
	component.ports["f"] = dmux3.GetOutput("b")
	component.ports["g"] = dmux3.GetOutput("c")
	component.ports["h"] = dmux3.GetOutput("d")
}

func calculateDMux256Way(component *Component) {
	in := component.ports["in"]
	sel := component.ports["sel"]

	selInt, _ := strconv.ParseInt(utils.BinToString(sel), 2, 64)
	out := make([]bool, component.outputs["out"])
	out[selInt] = in[0]

	component.ports["out"] = out
}
