package component_old

import (
	"computer-simulator/pkg/utils"
	"fmt"
	"testing"
)

func Test_DFF(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in", "clockIn"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"01", "0"},
			{"00", "0"},
			{"10", "0"},

			{"01", "0"},
			{"10", "0"},

			{"11", "1"},
			{"10", "1"},
			{"00", "1"},

			{"11", "1"},
			{"00", "1"},

			{"01", "0"},

			{"00", "0"},
			{"01", "0"},
		},
	}
	c := DFF.Clone()
	checkComponent(t, c, testSet)
}

func Test_Bit(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in", "load", "clockIn"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"001", "0"},
			{"000", "0"},

			{"011", "0"},
			{"001", "0"},
			{"110", "0"},
			{"001", "0"},

			{"111", "1"},
			{"100", "1"},

			{"001", "1"},
			{"100", "1"},

			{"001", "1"},
			{"000", "1"},
			{"010", "1"},

			{"011", "0"},
			{"100", "0"},
		},
	}
	c := Bit.Clone()
	checkComponent(t, c, testSet)
}

func Test_Register(t *testing.T) {

	testSet := TestSet{
		inputsNames:  []string{"in", "load", "clockIn"},
		outputsNames: []string{"out"},
		data: []TestEntry{
			{"00 01", "00"},
			{"01 11", "01"},
			{"00 10", "01"},
			{"10 10", "01"},

			{"00 01", "01"},
			{"00 00", "01"},
			{"11 00", "01"},

			{"00 01", "01"},
			{"10 00", "01"},

			{"10 11", "10"},
			{"00 00", "10"},
		},
	}
	c := Register.WithSize(2)
	checkComponent(t, c, testSet)
}

func Test_RAM16K(t *testing.T) {
	toWrite := "110"
	data := []TestEntry{}
	for i := 0; i < 16384; i += 512 {
		binAddress := utils.NumToBinString(i, 14)
		data = append(data, []TestEntry{
			{fmt.Sprintf("%s 1 %s 1", toWrite, binAddress), toWrite},
			{fmt.Sprintf("%s 0 %s 0", toWrite, binAddress), toWrite},
			{fmt.Sprintf("%s 0 %s 1", "000", binAddress), toWrite},
			{fmt.Sprintf("%s 0 %s 0", "000", binAddress), toWrite},
		}...)
	}
	testSet := TestSet{
		inputsNames:  []string{"in", "load", "address", "clockIn"},
		outputsNames: []string{"out"},
		data:         data,
	}
	c := RAM16K.WithSize(3)
	checkComponent(t, c, testSet)
}
